import { Component, OnDestroy } from '@angular/core';
import { navItems, navItems_buyer, navItems_seller, navItems_land_registry } from './../../_nav';
import { MyAuthService } from '../../service/myauth.service';
import { Router } from '@angular/router';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;

  public currentUserType = Globals.getUserType();
  public currentLoginName = Globals.getLoginName();

  constructor(
    private _myauthservice: MyAuthService,
    private _router: Router
  ) {

    console.log("Inside Default Layout:" + Globals.getUserType());
    if(Globals.getUserType() == CustomGlobalConstants.CONSTANT_BUYER){
      this.navItems = navItems_buyer;
    } else if(Globals.getUserType() == CustomGlobalConstants.CONSTANT_SELLER){
      this.navItems = navItems_seller;
    } else if(Globals.getUserType() == CustomGlobalConstants.CONSTANT_LAND_REGISTRY){
      this.navItems = navItems_land_registry;
    }

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  mylogout() {
    this._myauthservice.logout();
  }

}
