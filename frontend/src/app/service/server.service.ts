import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomLogger } from '../others/utils/CustomLogger';
import { CustomGlobalConstants } from '../others/utils/CustomGlobalConstants';
import { Incentive } from '../others/models/incentive.model';
import { UserProfile } from '../others/models/userProfile.model';
import { Globals } from '../others/models/Globals';
import { ToastrService } from 'ngx-toastr';
import { Property } from '../others/models/property.model';
import { Buyer } from '../others/models/buyer.model';
import { Seller } from '../others/models/seller.model';
import { SellProperty } from '../others/models/sellProperty.model';
// import { Buyproperty } from '../others/models/buyproperty.model';
import { Buyproperty } from '../others/models/buyproperty.model';
import { map } from 'rxjs/operators';


@Injectable()
export class ServerService {
  HOST_URL = "http://localhost:3000/common";

  me: any = Globals.getVendorId();
  constructor(private http: HttpClient, private toastrService: ToastrService) {
  }


  //authenticate user
  isUserAuthenticated(user) {
    return this.http.post(this.HOST_URL + "/authenticateUser", user);
  }
  saveNewUser(user) {
    return this.http.post(this.HOST_URL + "/saveNewUser", user);
  }

  //properties
  getPropertiesAvailableForSale() {
    return this.http.get(this.HOST_URL + "/getSpecificProperties/" + CustomGlobalConstants.CONSTANT_REALTOR + "/" + null);
  }

  getAllProperties() {
    return this.http.get(this.HOST_URL + "/getAllProperties");
  }

  saveProperty(property: Property) {
    return this.http.post(this.HOST_URL + "/saveProperty", property);
  }

  updateProperty(property: Property) {
    return this.http.post(this.HOST_URL + "/updateProperty", property);
  }

  getPropertyById(property_uuid) {
    return this.http.get(this.HOST_URL + "/getPropertyById/" + property_uuid);
  }

  //get specific seller/realtor properties
  getSpecificProperties(userType, id) {
    return this.http.get(this.HOST_URL + "/getSpecificProperties/" + userType + "/" + id);
  }
  getSpecificUserTypeId(userType, user_uuid) {
    return this.http.get(this.HOST_URL + "/getSpecificUserTypeId/" + userType + "/" + user_uuid);
  }
  //buyers
  getAllBuyers() {
    return this.http.get(this.HOST_URL + "/getAllBuyers");
  }

  saveBuyer(buyer: Buyer) {
    return this.http.post(this.HOST_URL + "/saveBuyer", buyer);
  }

  getSingleBuyer(buyer_uuid) {
    return this.http.get(this.HOST_URL + "/getSingleBuyer/" + buyer_uuid);
  }
  //sellers
  getAllSellers() {
    return this.http.get(this.HOST_URL + "/getAllSellers");
  }

  saveSeller(seller: Seller) {
    return this.http.post(this.HOST_URL + "/saveSeller", seller);
  }
  getSingleSeller(seller_uuid) {
    return this.http.get(this.HOST_URL + "/getSingleSeller/" + seller_uuid);
  }


  //property report
  saveBuyproperty(buyproperty: Buyproperty) {
    return this.http.post(this.HOST_URL + "/saveBuyproperty", buyproperty);
  }
  updateBuyproperty(buyproperty: Buyproperty) {
    return this.http.post(this.HOST_URL + "/updateBuyproperty", buyproperty);
  }
  getAllBuyproperties() {
    return this.http.get(this.HOST_URL + "/getAllBuyproperties");
  }
  getBuyPropertyById(buyproperty_uuid) {
    return this.http.get(this.HOST_URL + "/getBuyPropertyById/" + buyproperty_uuid);
  }
  getBuyersBiddedProperties(buyer_uuid) {
    return this.http.get(this.HOST_URL + "/getBuyersBiddedProperties/" + buyer_uuid);
  }
  getSoldPropertiesOfSeller(seller_uuid) {
    return this.http.get(this.HOST_URL + "/getSoldPropertiesOfSeller/" + seller_uuid);
  }

  //sell property
  getSpecificPropertyReport(userType, id) {
    return this.http.get(this.HOST_URL + "/getSpecificPropertyReport/" + userType + "/" + id);
  }


  //sell properties
  getAllSellProperties() {
    return this.http.get(this.HOST_URL + "/getAllSellProperties");
  }

  saveSellProperty(sellProperty: SellProperty) {
    return this.http.post(this.HOST_URL + "/saveSellProperty", sellProperty);
  }

  getSoldProperties(userType, user_uuid) {
    return this.http.get(this.HOST_URL + "/getSoldProperties/" + userType + "/" + user_uuid);
  }


  //land registry stuffs
  getLandRegistrySpecificPropertiesCount(approvedFlag){
    return this.http.get(this.HOST_URL + "/getLandRegistrySpecificPropertiesCount/" + approvedFlag);
  }


  getLatLongFromAddress(address) {
    // let GEOCODE_URL = "https://geocode.xyz/bhuj?json=1";
    return this.http.get("https://geocode.xyz/" + address + "?json=1");
    // let url = "https://geocode.xyz/Hauptstr.,+57632+Berzhausen?json=1";
    // return this.http.get(url);
  }

  
  downloadFILE(url): any {
    this.http.get(url).pipe(map(data => {})).subscribe(result => {
      console.log(result);
    });

    // return this.http.get(url, { responseType: 'blob' }).map(
    //   (res) => {
    //     return new Blob([res.blob()], { type: 'application/pdf' });
    //   });
  }

  //////////////////////

  /**
   * Get all incentives of the particular vendor
   * @param filter 
   * @param INCENTIVE_TYPE 
   */
  getAllIncentives(vendorId, INCENTIVE_TYPE) {
    CustomLogger.logString("SServer: getIncentives: vendorID: " + vendorId);
    let url = this.HOST_URL + '/incentives/getAllIncentives/' + vendorId + "/" + INCENTIVE_TYPE;
    CustomLogger.logString("URL to access: " + url);
    return this.http.get(url);
  }

  /**
   * Get all incentives of the particular vendor
   * @param filter 
   * @param INCENTIVE_TYPE 
   * /
  getAllIncentives_old(INCENTIVE_TYPE) {
    let vendorID = this.me.uuid;
    CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
    let url = this.HOST_URL + '/vendors/allIncentives/' + vendorID + "/" + INCENTIVE_TYPE;
    CustomLogger.logString("URL to access: " + url);
    return this.http.get(url);
  }
 

  /**
   * 
   * @param incentiveType - reward/coupon/...
   * @param searchNameString - search name string
   * @param searchType - created/expired/....
   */
  getSpecifcIncentives(vendorID, incentiveType, searchNameString: string, primarySearchType: string, searchType: string, searchStartTimeNumber: number, searchEndTimeNumber: number) {
    // let vendorID = this.me.uuid;
    CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
    if (!searchNameString || searchNameString.trim() === "") searchNameString = "NULL";
    if (!primarySearchType) primarySearchType = "ALL";
    if (!searchType) searchType = "ALL";

    let urlStr = this.HOST_URL + '/incentives/getSpecificSearchIncentives/' + incentiveType + "/" + vendorID + '/' + searchNameString + '/' + primarySearchType + '/' + searchType + '/' + searchStartTimeNumber +
      '/' + searchEndTimeNumber;
    CustomLogger.logString("urlStr to send: " + urlStr);
    return this.http.get(urlStr);
  }

  /**
     * 
     * @param incentiveType - reward/coupon/...
     * @param searchNameString - search name string
     * @param searchType - created/expired/....
     */
  // getSpecifcIncentives_old(incentiveType, searchNameString: string, primarySearchType: string, searchType: string, searchStartTimeNumber: number, searchEndTimeNumber: number) {
  //   let vendorID = this.me.uuid;
  //   CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
  //   if (!searchNameString || searchNameString.trim() === "") searchNameString = "NULL";
  //   if (!primarySearchType) primarySearchType = "ALL";
  //   if (!searchType) searchType = "ALL";

  //   let urlStr = this.HOST_URL + '/vendors/incentives/' + incentiveType + "/" + vendorID + '/' + searchNameString + '/' + primarySearchType + '/' + searchType + '/' + searchStartTimeNumber +
  //     '/' + searchEndTimeNumber;
  //   CustomLogger.logString("urlStr to send: " + urlStr);
  //   return this.http.get(urlStr);
  // }

  fetchIncentive(id) {
    return this.http.get(this.HOST_URL + '/incentives/getSingleIncentive/' + id);
  }

  // fetchIncentive_old(id) {
  //   return this.http.get(this.HOST_URL + '/coupons/' + id + '/details');
  // }

  fetchCustomerDetails(uuid) {
    return this.http.get(this.HOST_URL + '/vendors/customer/' + uuid);
  }

  // fetchMyDetails() {
  //   CustomLogger.logString("server: fetchMyDetails");
  //   return new Promise(function (resolve, reject) {
  //     if (this.me.uuid) resolve(this.me);
  //     CustomLogger.logString("server: host-url:" + this.HOST_URL);
  //     this.http.get(this.HOST_URL + '/users/me').subscribe(
  //       data => {
  //         this.me = data['data'];
  //         CustomLogger.logString("server: fetchMyDetails this.me:");
  //         CustomLogger.logObj(this.me);
  //         if (!this.me) {
  //           this.me = {
  //             uuid: 'dummy'
  //           }
  //         }
  //         resolve(this.me);
  //       },
  //       error => {
  //         CustomLogger.logObj(error);
  //         reject(error);
  //       }
  //     );
  //   }.bind(this));

  // }

  registerNewCustomer(body) {
    return this.http.post(this.HOST_URL + '/vendors/' + this.me.uuid + '/shop/registers', body);
  }

  fetchAllRegisteredCustomers() {
    return this.http.get(this.HOST_URL + '/vendors/' + this.me.uuid + '/registers/customers');
  }

  uploadImage(body) {
    return this.http.post(this.HOST_URL + '/common/images', body);
  }

  fetchImage(belongs_to, type) {
    return this.http.get(this.HOST_URL + '/common/images/' + belongs_to + '/images?type=' + type);
  }

  createBatchIncentive(body) {
    return this.http.post(this.HOST_URL + '/incentives/createIncentivesInBatch', body);
  }

  createIncentive(incentive: Incentive) {
    // CustomLogger.logString("createIncentive: this.me.uuid:::: " + this.me.uuid);
    // body.vendor_uuid = this.me.uuid;
    // return this.http.post(this.HOST_URL + '/coupons', incentive);
    return this.http.post(this.HOST_URL + '/incentives/addIncentive', incentive);
  }

  deleteIncentive(body) {
    body.vendor_uuid = this.me.uuid || null
    return this.http.post(this.HOST_URL + '/incentives/delete', body);
  }

  updateIncentive(body) {
    return this.http.post(this.HOST_URL + '/incentives/updateIncentive', body);
  }

  // updateIncentive_old(body) {
  //   //body.vendor_uuid = this.me.uuid
  //   CustomLogger.logString("updateIncentive: meeeee:" + this.me.uuid);
  //   return this.http.post(this.HOST_URL + '/coupons/update', body);
  // }

  // updateCustomer(body) {
  //   //body.vendor_uuid = this.me.uuid
  //   return this.http.post(this.HOST_URL + '/coupons', body);
  // }

  updateProfile(body) {
    return this.http.post(this.HOST_URL + '/vendors/updateProfile', body);
  }

  fetchProfile() {
    return this.http.get(this.HOST_URL + '/vendors/' + this.me.uuid + '/profile');
  }

  // getIncentiveTempId() {
  //   return new Promise(function (resolve, reject) {
  //     this.http.get(this.HOST_URL + '/coupons/temp/tempId').subscribe(
  //       data => {
  //         resolve(data.data.uuid);
  //       },
  //       error => {
  //         reject(error);
  //       }
  //     );
  //   }.bind(this));
  // }

  // createCouponsInBatch(body) {
  //   body.createdBy = this.me.uuid
  //   console.log('body from server service:' + JSON.stringify(body));
  //   return this.http.post(this.HOST_URL + '/coupons/createCouponsInBatch', body);
  // }

  // getAllBatchCoupons(filter) {
  //   //let vendorID = this.me.uuid;
  //   return this.http.get(this.HOST_URL + '/coupons/getAllBatchCoupons');
  // }

  //get all rewards specific details 
  getAllIncentiveCountsFromDB(vendorID, incentiveType, searchStartTime, searchEndTime): Observable<any> {
    // CustomLogger.logString("vendor:" + vendorID);
    // return this.http.get(this.HOST_URL + '/coupons/getAllIncentivesSpecificDetails/' + vendorID + "/" + incentiveType + '/' + searchStartTime + "/" + searchEndTime);
    let url = this.HOST_URL + '/incentives/getAllIncentiveCounts/' + vendorID + "/" +
      incentiveType + "/" + searchStartTime + "/" + searchEndTime;
    CustomLogger.logString("getAllIncentiveCountsFromDB: URL:" + url);
    return this.http.get(url);
  }


  // //get all rewards specific details
  // getAllRewardsSpecificDetailsFromDB(vendorID, searchStartTime, searchEndTime): Observable<any> {
  //   CustomLogger.logString("getAllRewardsSpecificDetailsFromDB for vendor:" + vendorID);
  //   return this.http.get(this.HOST_URL + '/coupons/' + vendorID + "/" + searchStartTime + "/" + searchEndTime + '/getAllRewardsSpecificDetails');    
  // }

  //get suggested rewards
  // getSuggestedRewardsFromDB(): Observable<any> {
  //   // let vendorID = this.me.uuid || null;
  //   return this.http.get(this.HOST_URL + '/coupons/' + vendorID + '/getSuggestedRewards');
  // }

  createIncentivesFromUpload(incentiveArr): Observable<any> {
    //body.createdBy = this.me.uuid
    // console.log('body from server service:' + JSON.stringify(incentiveArr));
    return this.http.post(this.HOST_URL + '/incentives/createIncentivesFromUpload', incentiveArr);
  }


  // createIncentivesFromUpload(incentiveArr): Observable<any> {
  //   //body.createdBy = this.me.uuid
  //   console.log('body from server service:' + JSON.stringify(incentiveArr));
  //   return this.http.post(this.HOST_URL + '/coupons/createIncentivesFromUpload', incentiveArr);
  // }

  // getCurrentVendorUUID() {
  //   return this.me.uuid;
  // }

  /**
   * 
   * @param incentiveUuidsArr 
   */
  deactivateBatchIncentives(vendorId, incentiveUuidsArr): Observable<any> {
    CustomLogger.logString("About to call server for deactivating many incentives.");
    return this.http.post(this.HOST_URL + '/vendors/deactivateBatchIncentives/' + vendorId, incentiveUuidsArr);
  }


  fetchUserProfile(userId) {
    return this.http.get(this.HOST_URL + '/users/profile/' + userId);
  }

  fetchVendorDetails(vendorId) {
    let url = this.HOST_URL + '/vendors/getVendorProfile/' + vendorId;
    CustomLogger.logString("Url for fetch vendor profile: " + url);
    return this.http.get(url);
  }

  userUpdateCompletedProfile(userId, updateFlag) {
    return this.http.post(this.HOST_URL + '/users/updateCompletedProfile/' + userId, { "updateFlag": updateFlag });
  }

  fetchVendorDetails_orig() {
    CustomLogger.logString("server: fetchVendorDetails");
    return new Promise(function (resolve, reject) {
      if (this.me.uuid) resolve(this.me);
      CustomLogger.logString("server: host-url:" + this.HOST_URL);
      this.http.get(this.HOST_URL + '/users/me').subscribe(
        data => {
          this.me = data['data'];
          CustomLogger.logString("server: fetchVendorDetails this.me:");
          CustomLogger.logObj(this.me);
          if (!this.me) {
            this.me = {
              uuid: 'dummy'
            }
          }
          resolve(this.me);
        },
        error => {
          CustomLogger.logObj(error);
          reject(error);
        }
      );
    }.bind(this));

  }


  getVendorGlobals(token) {
    let url = this.HOST_URL + '/users/getGlobalData/' + token;
    CustomLogger.logString("getvendorglobals url: " + url);
    return this.http.get(url);
  }

  ////////////////alerts
  showMessage(message) {
    console.log("inside  showSuccessMessage ");
    this.toastrService.success(message, "some title");
  }


}//end of class
