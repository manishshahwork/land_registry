import { Injectable } from '@angular/core';
import { UserProfile } from '../others/models/userProfile.model';
import { CustomLogger } from '../others/utils/CustomLogger';
import { HttpClient } from '@angular/common/http';
import { CustomGlobalConstants } from '../others/utils/CustomGlobalConstants';


@Injectable()
export class MyAuthService {

  constructor(private http: HttpClient) { }

  
  /////////
  authenticateGoogleUser(userProfile: UserProfile){
    CustomLogger.logString("About to call server for authenticating google user.");
    // let vendorId = this.me.uuid || null;
    return this.http.post(CustomGlobalConstants.HOST_URL + '/users/authenticateGoogleUser', userProfile);    
  }


  loggedIn(){
    return !!localStorage.getItem(CustomGlobalConstants.KEY_TOKEN);
  }

  logout(){
    localStorage.clear();
  }
}
