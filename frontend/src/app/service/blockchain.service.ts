import { Injectable } from '@angular/core';
import { Buyer } from '../others/models/buyer.model';
import { CustomLogger } from '../others/utils/CustomLogger';
import { Seller } from '../others/models/seller.model';
import { Property } from '../others/models/property.model';
import { SellProperty } from '../others/models/sellProperty.model';
import { HttpClient } from '@angular/common/http';
import { Buyproperty } from '../others/models/buyproperty.model';


@Injectable({
  providedIn: 'root'
})
export class BlockchainService {

  HOST_URL = "http://ec2-13-232-190-139.ap-south-1.compute.amazonaws.com:3000/api/";
  constructor(private http: HttpClient) { }

  addClassObj(actualObj, className) {
    let originalObj = JSON.stringify(actualObj);
    let tmpObj = JSON.parse(originalObj);
    tmpObj.$class = className;
    // originalObj = JSON.stringify(tmpObj);
    return tmpObj;
  }

  addBuyer(buyer: Buyer) {
    let jsonBuyer = this.addClassObj(buyer, "net.biz.digitalPropertyNetwork.Buyer");
    CustomLogger.logStringWithObject("The following Buyer will be added to Blockchain", jsonBuyer);
    return this.http.post(this.HOST_URL + "Buyer", jsonBuyer);
  }

  addSeller(seller: Seller) {
    let jsonObj = this.addClassObj(seller, "net.biz.digitalPropertyNetwork.Seller");
    CustomLogger.logStringWithObject("The following Seller will be added to Blockchain", jsonObj);
    return this.http.post(this.HOST_URL + "Seller", jsonObj);
  }
  

  addProperty(property: Property) {
    //call block chain method to add Property
    let jsonProperty = this.addClassObj(property, "net.biz.digitalPropertyNetwork.Property");
    CustomLogger.logStringWithObject("The following Property will be added to Blockchain", jsonProperty);
    return this.http.post(this.HOST_URL + "Property", jsonProperty);
  }
  updateProperty(property: Property) {
    //call block chain method to add Property
    let jsonProperty = this.addClassObj(property, "net.biz.digitalPropertyNetwork.Property");
    CustomLogger.logStringWithObject("The following Property will be updated in Blockchain", jsonProperty);
    return this.http.put(this.HOST_URL + "Property", jsonProperty);
  }

  //sell proprety
  addSellProperty(sellProperty: SellProperty) {
    //call block chain method to sell property    
    let jsonObj = this.addClassObj(sellProperty, "net.biz.digitalPropertyNetwork.SellProperty");
    CustomLogger.logStringWithObject("The following sellProperty will be added to Blockchain", jsonObj);
    return this.http.post(this.HOST_URL + "SellProperty", jsonObj);
  }
  updateSellProperty(sellProperty: SellProperty) {
    //call block chain method to sell property    
    let jsonObj = this.addClassObj(sellProperty, "net.biz.digitalPropertyNetwork.SellProperty");
    CustomLogger.logStringWithObject("The following sellProperty will be updated to Blockchain", jsonObj);
    return this.http.put(this.HOST_URL + "SellProperty", jsonObj);
  }


  addBuyProperty(buyproperty: Buyproperty) {
    //call block chain method to sell property    
    let jsonObj = this.addClassObj(buyproperty, "net.biz.digitalPropertyNetwork.Buyproperty");
    CustomLogger.logStringWithObject("The following buyproperty will be added to Blockchain", jsonObj);
    return this.http.post(this.HOST_URL + "Buyproperty", jsonObj);
  }
  updateBuyProperty(buyproperty: Buyproperty) {
    //call block chain method to sell property    
    let jsonObj = this.addClassObj(buyproperty, "net.biz.digitalPropertyNetwork.Buyproperty");
    CustomLogger.logStringWithObject("The following buyproperty will be updated to Blockchain", jsonObj);
    return this.http.put(this.HOST_URL + "Buyproperty", jsonObj);
  }

  getBuyersList() {
    //call block chain method to get all buyers
    CustomLogger.logString("Blockchain code to Get Buyers");
    return this.http.get(this.HOST_URL + "Buyer");
  }
  getSellersList() {
    //call block chain method to get all Sellers
    CustomLogger.logString("Blockchain code to Get Sellers");
    return this.http.get(this.HOST_URL + "Seller");
  }
  getPropertiesList() {
    //call block chain method to get all Properties
    CustomLogger.logString("Blockchain code to Get Properties");
    return this.http.get(this.HOST_URL + "Property");
  }
  getSellPropertiesList() {
    //call block chain method to get all sold Properties
    CustomLogger.logString("Blockchain code to Get Sold Properties");
    return this.http.get(this.HOST_URL + "SellProperty");
  }

}
