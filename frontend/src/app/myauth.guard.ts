import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { MyAuthService } from './service/myauth.service';
import { Globals } from './others/models/Globals';


@Injectable()
export class MyAuthGuard implements CanActivate {

  constructor(
    private _router: Router,
    private _myauthService: MyAuthService
  ) { }

  canActivate(): boolean {
    // console.log("ccccccccccccccc");
    if (Globals.getUserId()) return true;
    return false;
  }
} 
