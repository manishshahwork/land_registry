export interface NavData {
  name?: string,
  url?: string,
  icon?: string,
  badge?: any,
  title?: boolean,
  children?: any,
  variant?: string,
  attributes?: object,
  divider?: boolean,
  class?: string,
}


export const navItems_buyer: NavData[] = [

  {
    name: 'Dashboard',
    url: '/ourdashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'All Listings',
    url: '/listing',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Properties For Sale',
        url: '/listing/propertylisting',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Buy Property',
    url: '/buyproperty',
    icon: 'icon-speedometer'
  },
  {
    name: 'Requested Properties',
    url: '/propertyReport',
    icon: 'icon-star'
  },
  {
    name: 'Bought Properties',
    url: '/soldproperty',
    icon: 'icon-pie-chart'
  },
];


export const navItems_seller: NavData[] = [

  {
    name: 'Dashboard',
    url: '/ourdashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'All Listings',
    url: '/listing',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Properties For Sale',
        url: '/listing/propertylisting',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Add Property',
    url: '/property',
    icon: 'icon-bell'
  },
  // {
  //   name: 'Sell Property',
  //   url: '/sellproperty',
  //   icon: 'icon-drop'
  // },
  {
    name: 'Buyers Interested',
    url: '/propertyReport',
    icon: 'icon-star'
  },
  {
    name: 'Sold Properties',
    url: '/soldproperty',
    icon: 'icon-pie-chart'
  },
];


export const navItems_land_registry: NavData[] = [

  {
    name: 'Dashboard',
    url: '/ourdashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Property Report',
    url: '/propertyReport',
    icon: 'icon-star'
  },
];

export const navItems_bank: NavData[] = [

  {
    name: 'Dashboard',
    url: '/ourdashboard',
    icon: 'icon-speedometer'
  }
];

export const navItems: NavData[] = [

  {
    name: 'Dashboard',
    url: '/ourdashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'All Listings',
    url: '/listing',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Properties For Sale',
        url: '/listing/propertylisting',
        icon: 'icon-puzzle'
      },
      {
        name: 'Buyer Listing',
        url: '/listing/buyerlisting',
        icon: 'icon-puzzle'
      },
      {
        name: 'Seller Listing',
        url: '/listing/sellerlisting',
        icon: 'icon-puzzle'
      },     
    ]
  },

  {
    name: 'Add Buyer',
    url: '/buyer',
    icon: 'icon-cursor'
  },
  {
    name: 'Add Seller',
    url: '/seller',
    icon: 'icon-ban'
  },
  {
    name: 'Add Property',
    url: '/property',
    icon: 'icon-bell'
  },
  {
    name: 'Property Transactions',
    url: '/propertyReport',
    icon: 'icon-star'
  },

////////////////////////////////////////////////////////////////

//   {
//     name: 'Rewards',
//     url: '/incentives/reward',
//     icon: 'icon-puzzle'
//   },
//   {
//     name: 'Coupons',
//     url: '/incentives/coupon',
//     icon: 'icon-star'
//   },
//   {
//     name: 'Campaigns',
//     url: '/campaigns',
//     icon: 'icon-pie-chart'
//   },
//   {
//     name: 'Customers',
//     url: '/customer',
//     icon: 'icon-pie-chart'
//   },
//   {
//     name: 'Billing',
//     url: '/billing',
//     icon: 'icon-pie-chart'
//   },
//   {
//     name: 'Support',
//     url: '/support',
//     icon: 'icon-pie-chart'
//   },
//   {
//     name: 'Profile',
//     url: '/profile',
//     icon: 'icon-puzzle',
//     children: [
//       {
//         name: 'My Profile',
//         url: '/profile/myprofile',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Business Profile',
//         url: '/profile/businessprofile',
//         icon: 'icon-puzzle'
//       }
//     ]
//   },





  


// //////////////////////////////////////////////////////////////  

//   {
//     name: 'Dashboard',
//     url: '/dashboard',
//     icon: 'icon-speedometer',
//     badge: {
//       variant: 'info',
//       text: 'NEW'
//     }
//   },
//   {
//     title: true,
//     name: 'Theme'
//   },
//   {
//     name: 'Colors',
//     url: '/theme/colors',
//     icon: 'icon-drop'
//   },
//   {
//     name: 'Typography',
//     url: '/theme/typography',
//     icon: 'icon-pencil'
//   },
//   {
//     title: true,
//     name: 'Components'
//   },
//   {
//     name: 'Base',
//     url: '/base',
//     icon: 'icon-puzzle',
//     children: [
//       {
//         name: 'Cards',
//         url: '/base/cards',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Carousels',
//         url: '/base/carousels',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Collapses',
//         url: '/base/collapses',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Forms',
//         url: '/base/forms',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Pagination',
//         url: '/base/paginations',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Popovers',
//         url: '/base/popovers',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Progress',
//         url: '/base/progress',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Switches',
//         url: '/base/switches',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Tables',
//         url: '/base/tables',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Tabs',
//         url: '/base/tabs',
//         icon: 'icon-puzzle'
//       },
//       {
//         name: 'Tooltips',
//         url: '/base/tooltips',
//         icon: 'icon-puzzle'
//       }
//     ]
//   },
//   {
//     name: 'Buttons',
//     url: '/buttons',
//     icon: 'icon-cursor',
//     children: [
//       {
//         name: 'Buttons',
//         url: '/buttons/buttons',
//         icon: 'icon-cursor'
//       },
//       {
//         name: 'Dropdowns',
//         url: '/buttons/dropdowns',
//         icon: 'icon-cursor'
//       },
//       {
//         name: 'Brand Buttons',
//         url: '/buttons/brand-buttons',
//         icon: 'icon-cursor'
//       }
//     ]
//   },
//   {
//     name: 'Charts',
//     url: '/charts',
//     icon: 'icon-pie-chart'
//   },
//   {
//     name: 'Icons',
//     url: '/icons',
//     icon: 'icon-star',
//     children: [
//       {
//         name: 'CoreUI Icons',
//         url: '/icons/coreui-icons',
//         icon: 'icon-star',
//         badge: {
//           variant: 'success',
//           text: 'NEW'
//         }
//       },
//       {
//         name: 'Flags',
//         url: '/icons/flags',
//         icon: 'icon-star'
//       },
//       {
//         name: 'Font Awesome',
//         url: '/icons/font-awesome',
//         icon: 'icon-star',
//         badge: {
//           variant: 'secondary',
//           text: '4.7'
//         }
//       },
//       {
//         name: 'Simple Line Icons',
//         url: '/icons/simple-line-icons',
//         icon: 'icon-star'
//       }
//     ]
//   },
//   {
//     name: 'Notifications',
//     url: '/notifications',
//     icon: 'icon-bell',
//     children: [
//       {
//         name: 'Alerts',
//         url: '/notifications/alerts',
//         icon: 'icon-bell'
//       },
//       {
//         name: 'Badges',
//         url: '/notifications/badges',
//         icon: 'icon-bell'
//       },
//       {
//         name: 'Modals',
//         url: '/notifications/modals',
//         icon: 'icon-bell'
//       }
//     ]
//   },
//   {
//     name: 'Widgets',
//     url: '/widgets',
//     icon: 'icon-calculator',
//     badge: {
//       variant: 'info',
//       text: 'NEW'
//     }
//   },
//   {
//     divider: true
//   },
//   {
//     title: true,
//     name: 'Extras',
//   },
//   { 
//     name: 'Pages',
//     url: '/pages',
//     icon: 'icon-star',
//     children: [
//       {
//         name: 'Login',
//         url: '/login',
//         icon: 'icon-star'
//       },
//       {
//         name: 'Register',
//         url: '/register',
//         icon: 'icon-star'
//       },
//       {
//         name: 'Error 404',
//         url: '/404',
//         icon: 'icon-star'
//       },
//       {
//         name: 'Error 500',
//         url: '/500',
//         icon: 'icon-star'
//       }
//     ]
//   },
//   {
//     name: 'Disabled',
//     url: '/dashboard',
//     icon: 'icon-ban',
//     badge: {
//       variant: 'secondary',
//       text: 'NEW'
//     },
//     attributes: { disabled: true },
//   },



////////////////////////////////////////////////////////////////


];
