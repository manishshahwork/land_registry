import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { CrudIncentiveComponent } from './views/crud-incentive/crud-incentive.component';
import { IncentivesComponent } from './views/incentives/incentives.component';
import { MyAuthGuard } from './myauth.guard';



export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'buyer', 
        loadChildren: './views/buyer/buyer.module#BuyerModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'seller', 
        loadChildren: './views/seller/seller.module#SellerModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'propertyReport', 
        loadChildren: './views/propertyReport/propertyReport.module#PropertyReportModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'property', 
        loadChildren: './views/property/property.module#PropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'property/:property_uuid', 
        loadChildren: './views/property/property.module#PropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'soldproperty', 
        loadChildren: './views/soldproperty/soldproperty.module#SoldpropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'sellproperty', 
        loadChildren: './views/sellproperty/sellproperty.module#SellpropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'sellproperty/:buyproperty_uuid', 
        loadChildren: './views/sellproperty/sellproperty.module#SellpropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'buyproperty', 
        loadChildren: './views/buyproperty/buyproperty.module#BuypropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'buyproperty/:buyproperty_uuid', 
        loadChildren: './views/buyproperty/buyproperty.module#BuypropertyModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'listing', 
        loadChildren: './views/listing/listing.module#ListingModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'approveproperty/:buyproperty_uuid', 
        loadChildren: './views/approveproperty/approveproperty.module#ApprovepropertyModule',
        canActivate: [MyAuthGuard]
      },


      
      {
        path: 'base',
        loadChildren: './views/base/base.module#BaseModule'
      },
      {
        path: 'buttons',
        loadChildren: './views/buttons/buttons.module#ButtonsModule'
      },
      {
        path: 'charts',
        loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'notifications',
        loadChildren: './views/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'theme',
        loadChildren: './views/theme/theme.module#ThemeModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'billing', 
        loadChildren: './views/billing/billing.module#BillingModule',
        canActivate: [MyAuthGuard]
      }, 
      {
        path: 'customer', 
        loadChildren: './views/customer/customer.module#CustomerModule',
        canActivate: [MyAuthGuard]
      },
      {
        path: 'support', 
        loadChildren: './views/support/support.module#SupportModule',
        canActivate: [MyAuthGuard]
      }, 
      {
        path: 'campaigns', 
        loadChildren: './views/campaigns/campaigns.module#CampaignsModule',
        canActivate: [MyAuthGuard]
      },      
      {
        path: 'ourdashboard', 
        loadChildren: './views/ourdashboard/ourdashboard.module#OurdashboardModule',
        canActivate: [MyAuthGuard]
      },      
      {
        path: 'incentives/:incentiveType', 
        component: IncentivesComponent,
        canActivate: [MyAuthGuard]
      },
      {
        path: 'incentives/add/:incentiveType',
        component: CrudIncentiveComponent,
        canActivate: [MyAuthGuard]
      },
      {
        path: 'incentives/modify/:incentiveType/:crudType/:id',
        component: CrudIncentiveComponent,
        canActivate: [MyAuthGuard]
      },
      {
        path: 'profile', 
        loadChildren: './views/profile/profile.module#ProfileModule',
        canActivate: [MyAuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
 