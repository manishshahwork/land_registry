import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyComponent } from './property.component';
import { Property } from '../../others/models/property.model';

const routes: Routes = [
  {
    path: '',
    component: PropertyComponent,
    data: {
      title: 'Properties'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule{

 
} 
 