import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule, TabsModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { PropertyComponent } from './property.component';
import { PropertyRoutingModule } from './property-routing.module';
import { FormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';  

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    PropertyRoutingModule,
    FormsModule,
    TabsModule,
    LeafletModule.forRoot()
  ],
  declarations: [ PropertyComponent ]
})
export class PropertyModule { } 
