import { Component, OnInit } from '@angular/core';
import { Property } from '../../others/models/property.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockchainService } from '../../service/blockchain.service';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { latLng, tileLayer } from 'leaflet';
import * as L from 'leaflet';
import { Observable } from 'rxjs';
import { CustomMisc } from '../../others/utils/CustomMisc';

@Component({
  templateUrl: 'property.component.html',
  styleUrls: ['property.component.css']
})
export class PropertyComponent implements OnInit {

  map: L.Map;

  property: Property;
  isLandRegistryUserFlag = false;

  // isSellerAccessingFlag = true;
  htmlSpace = " ";
  sellerList: any;
  // isBuyerAccessingFlag = false;
  isEditableFlag = true;
  showDisplaySellerList = true;
  isNewPropertyFlag = true;

  //file related
  // htmlUploadedImageName: string= "";
  htmlImageLink: string = CustomGlobalConstants.DEFAULT_IMAGE_FILE_LINK;
  currentImageFile: File;
  uploadImageFlag: boolean = false;
  CURRENT_UPLOAD_FILE_NAME = "CURRENT_UPLOAD_FILE_NAME";

  //map related
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      })
    ],
    zoom: 10,
    center: latLng([38.639542, -121.511131])
  };

  onMapReady(map: L.Map) {
    this.map = map;
    setTimeout(() => {
      map.invalidateSize();
    }, 0);
  }

  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router, private blockchain: BlockchainService) { }

  ngOnInit() {
    this.property = new Property();
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_LAND_REGISTRY) {
      CustomLogger.logString("Property: Land Registry User Logged in.");
      this.isLandRegistryUserFlag = true;
    } else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_BUYER) {
      this.isEditableFlag = false;
      this.showDisplaySellerList = false;
    } 
    // else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_REALTOR) {
    //   this.isEditableFlag = false;
    // }
    this.activatedRoute.params.subscribe(
      params => {
        CustomLogger.logStringWithObject("Got params.... ", params);
        if (params.property_uuid) {
          this.service.getPropertyById(params.property_uuid).subscribe(
            data => {
              CustomLogger.logStringWithObject("getPropertyById Result  from db: ", data);
              this.property = data["data"];
              this.isNewPropertyFlag = false;
            },
            err => {
              CustomLogger.logStringWithObject("getPropertyById Error from db: ", err);
            }
          );
        }
      }
    );


    //currently seller can add property, so get the user id from it
    //check who the user is
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_SELLER) {
      CustomLogger.logStringWithObject("A seller is accessing ... : ", Globals.getUserType());
      this.property.seller_uuid = Globals.getUserTypeId();
      this.showDisplaySellerList = false;
    } else {
      //get sellers list from db
      this.service.getAllSellers().subscribe(
        data => {
          CustomLogger.logStringWithObject("getAllSellers Result from db: ", data);
          this.sellerList = data["data"];
        },
        err => {
          CustomLogger.logStringWithObject("Error from db: ", err);
        }
      );

      // if (Globals.shouldUseBlockchain()) {
      //   ////////////blockchain - start
      //   this.blockchain.getSellersList().subscribe(
      //     data1 => {
      //       CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
      //       this.sellerList = data1;
      //     },
      //     err1 => {
      //       CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
      //     }
      //   );
      //   ////////////blockchain - end
      // } else {
      //   //get sellers list from db
      //   this.service.getAllSellers().subscribe(
      //     data => {
      //       CustomLogger.logStringWithObject("getAllSellers Result from db: ", data);
      //       this.sellerList = data["data"];
      //     },
      //     err => {
      //       CustomLogger.logStringWithObject("Error from db: ", err);
      //     }
      //   );
      // }

    }
  }

  async onSubmit() {
    console.log("lllllllllllink:" + this.htmlImageLink);
    console.log("locallll : " + localStorage.getItem('htmlImageLink'));
    //upload files to server
    if (this.uploadImageFlag) {
      //temporary object to hold values
      let tmpProperty = this.property;
      await CustomMisc.uploadFileToRemoteServer(this.currentImageFile).then(function (dataLocation) {
        CustomLogger.logString("gottttttttttttt"); 
        CustomLogger.logObj(dataLocation);
        CustomLogger.logString("tmpProperty");
        CustomLogger.logObj(tmpProperty);
        CustomLogger.logStringWithObject("CURRENT_UPLOAD_FILE_NAME:", localStorage.getItem("CURRENT_UPLOAD_FILE_NAME"));
        //CustomLogger.logString("this.incentive");
        //CustomLogger.logObj(this.incentive);
        //this.incentive.image_link = dataLocation;
        tmpProperty.document_location_url = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
        // CustomLogger.logString("htmlUploadedImageName:" + this.htmlUploadedImageName);
        tmpProperty.document_name = localStorage.getItem("CURRENT_UPLOAD_FILE_NAME");
        // CustomLogger.logObj(tmpIncetive);
      });
      //convert back to original object
      this.property = tmpProperty;
      CustomLogger.logString("this.property");
      CustomLogger.logObj(this.property);
    }



    if (this.isNewPropertyFlag) {
      CustomLogger.logStringWithObject("Property to be saved: ", this.property);
      //get seller name from db
      this.service.getSingleSeller(this.property.seller_uuid).subscribe(
        data1 => {
          CustomLogger.logStringWithObject("getSingleSeller Result  from db: ", data1);
          let seller = data1["data"];
          this.property.seller_name = seller.first_name + " " + seller.middle_name + " " + seller.last_name;

          this.service.saveProperty(this.property).subscribe(
            data => {
              this.property.uuid = data["data"]["uuid"];
              CustomLogger.logStringWithObject("saveProperty Result from dbb : ", data);

              if (Globals.shouldUseBlockchain()) {
                ////////////blockchain - start
                this.blockchain.addProperty(this.property).subscribe(
                  data1 => {
                    CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
                    alert("Property added to blockchain");
                    this.router.navigate(["ourdashboard"]);
                  },
                  err1 => {
                    CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
                    this.router.navigate(["ourdashboard"]);
                  }
                );
                ////////////blockchain - end
              }

              this.router.navigate(["ourdashboard"]);
            },
            err => {
              CustomLogger.logStringWithObject("Error from db: ", err);
              this.router.navigate(["ourdashboard"]);
            }
          );

        },
        err1 => {
          CustomLogger.logStringWithObject("getSingleSeller Error from db: ", err1);
          this.router.navigate(["ourdashboard"]);
        }
      );

    } else {
      CustomLogger.logStringWithObject("Property to be updated: ", this.property);
      this.service.updateProperty(this.property).subscribe(
        data => {
          this.property.uuid = data["data"]["uuid"];
          CustomLogger.logStringWithObject("Result from dbb : ", data);

          // if (Globals.shouldUseBlockchain()) {
          //   ////////////blockchain - start
          //   this.blockchain.updateProperty(this.property).subscribe(
          //     data1 => {
          //       CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
          //       alert("Property updated in blockchain");
          //       this.router.navigate(["ourdashboard"]);
          //     },
          //     err1 => {
          //       CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
          //       this.router.navigate(["ourdashboard"]);
          //     }
          //   );
          //   ////////////blockchain - end
          // }
        },
        err => {
          CustomLogger.logStringWithObject("Error from db: ", err);
          this.router.navigate(["ourdashboard"]);
        }
      );
    }

  }


  onClickNewGIS() {
    CustomLogger.logStringWithObject("map::", this.map);
    CustomLogger.logString("get lat and long for address: " + this.property.property_address);

    this.service.getLatLongFromAddress(this.property.property_address).subscribe(
      data => {
        CustomLogger.logStringWithObject("Got reply... ", data);
      },
      err => {
        CustomLogger.logStringWithObject("Got error... ", err);
        this.map.panTo(new L.LatLng(38.639542, -121.511131)); 
      }
    );
  }

  async onFileUpload(event) {
    //let file: File = files.item(0);
    let imageFile = event.target.files[0];
    this.currentImageFile = imageFile;
    localStorage.setItem(this.CURRENT_UPLOAD_FILE_NAME, this.currentImageFile.name);
    ////this.uploadImageFile(file);
    CustomLogger.logString("will upload file::: ");
    CustomLogger.logObj(this.currentImageFile);
    this.uploadImageFlag = true; // flag required in order to update image on server
    // this.htmlUploadedImageName = this.currentImageFile.name;
    
    //preview the image

    var reader = new FileReader();
    reader.readAsDataURL(imageFile); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      // console.log("inside onload event...");
      this.htmlImageLink = event.target.result;
      // console.log("htmlImageLink:::' " + this.htmlImageLink);
    }
  }

  updateHTMLSpecificFields() {
    //update html image link
    this.htmlImageLink = this.property.document_location_url;
    // console.log("start time:::: ");
    //var htmlStartTime = this.getTime(this.incentive.start_time);
    // console.log(this.htmlStartTime);
    // this.htmlDateRange = this.htmlStartTime + " - " + this.htmlEndTime;

  }

  onClickDisplayFile(){
    CustomLogger.logString("this.property.document_location_url:" + this.property.document_location_url);
    // window.open(this.property.document_location_url); 
    // let tab = window.open();
    // this.service.downloadFILE(this.property.document_location_url).subscribe(data => {
    //     const fileUrl = URL.createObjectURL(data);
    //     tab.location.href = fileUrl;
    //   });
  }
}
