import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { SupportComponent } from './support.component';
import { SupportRoutingModule } from './support-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule, 
    SupportRoutingModule, 
    FormsModule
  ],
  declarations: [ SupportComponent ]
})
export class SupportModule { } 
