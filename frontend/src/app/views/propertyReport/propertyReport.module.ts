import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PropertyReportRoutingModule } from './propertyReport-routing.module';
import { PropertyReportComponent } from './propertyReport.component';
import { DataTableModule } from 'angular-6-datatable';
import { InlineEditorModule } from '@qontu/ngx-inline-editor';

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    PropertyReportRoutingModule,
    FormsModule,
    DataTableModule,
    InlineEditorModule
  ],
  declarations: [ PropertyReportComponent ]
})
export class PropertyReportModule { } 
