import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyReportComponent } from './propertyReport.component';

const routes: Routes = [
  {
    path: '',
    component: PropertyReportComponent,
    data: {
      title: 'Property Report'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyReportRoutingModule {} 
