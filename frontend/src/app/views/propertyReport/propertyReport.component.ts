import { Component, OnInit } from '@angular/core';
import { Seller } from '../../others/models/seller.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Router } from '@angular/router';
import { ServerService } from '../../service/server.service';
import { BlockchainService } from '../../service/blockchain.service';
import { User } from '../../others/models/user.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'propertyReport.component.html'
})
export class PropertyReportComponent implements OnInit {

  titleText = "Current Property Status";
  tableDataArr: any;
  isLandRegistryAccessingFlag = false;
  isRealtorAccessingFlag = false;
  isSellerAccessingFlag = false;
  isBuyerAccessingFlag = false;
  constructor(private service: ServerService, private router: Router, private blockchain: BlockchainService) { }

  ngOnInit() {
    CustomLogger.logString("Current user type id: " + Globals.getUserTypeId() + " and type:" + Globals.getUserType());
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_BUYER) {
      this.isBuyerAccessingFlag = true;
      this.titleText = "Properties Requested to buy";
    }
    else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_SELLER) {
      this.isSellerAccessingFlag = true;
      this.titleText = "Properties Interested by Buyers";
    }
    else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_REALTOR) {
      this.isRealtorAccessingFlag = true;
      this.titleText = "All Properties Transaction Status";
    }
    else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_LAND_REGISTRY) {
      this.isLandRegistryAccessingFlag = true;
      this.titleText = "Properties to be approved";
    }

    this.service.getSpecificPropertyReport(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
      data => {
        CustomLogger.logStringWithObject("getSpecificPropertyReport: Result from db: ", data);
        this.tableDataArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("getSpecificPropertyReport: Error from db: ", err);
      }
    );

  }

  onClickEdit(event) {
    CustomLogger.logStringWithObject("onClickEdit: ", event.target.value);
    this.router.navigate(["buyproperty", event.target.value]);
  }

  onClickView(event) {
    CustomLogger.logStringWithObject("onClickView: ", event.target.value);
    let v = event.target.value + "";
    let k = v.split("#", 2);
    CustomLogger.logString("K: " + k[0]);
    if (k[0] == CustomGlobalConstants.CONSTANT_CLOSED) {
      alert("Sorry! Cannot view property details of a Closed Deal.");
    } else if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_BUYER && (k[0] == CustomGlobalConstants.CONSTANT_CLOSED || k[0] == CustomGlobalConstants.CONSTANT_REQUEST || k[0] == CustomGlobalConstants.CONSTANT_CANCELLED)) {
      alert("Cannot view the property as its status is not valid.");
    } else {
      let property_uuid = k[1];
      this.router.navigate(["property", property_uuid]);
    }
  }

  onClickClose(event) {
    CustomLogger.logStringWithObject("onClickClose: ", event.target.value);
    let v = event.target.value + "";
    let k = v.split("#", 2);
    CustomLogger.logString("K: " + k[0]);
    if (k[0] == CustomGlobalConstants.CONSTANT_CLOSED) {
      alert("Sorry! Cannot close an already Closed Deal.");
    } else if (k[0] != CustomGlobalConstants.CONSTANT_APPROVED) {
      alert("Sorry! Cannot close this Deal as it is not yet approved by the Land Registry.");
    } else {
      let buyproperty_uuid = k[1];
      this.router.navigate(["sellproperty", buyproperty_uuid]);
    }
  }

  onClickApproval(event) {
    let buyproperty_uuid = event.target.value;
    this.router.navigate(["approveproperty", buyproperty_uuid]);
  }
}
