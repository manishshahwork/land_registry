import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { SellerRoutingModule } from './seller-routing.module';
import { SellerComponent } from './seller.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    SellerRoutingModule,
    FormsModule
  ],
  declarations: [ SellerComponent ]
})
export class SellerModule { } 
