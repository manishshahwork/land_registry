import { Component, OnInit } from '@angular/core';
import { Seller } from '../../others/models/seller.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Router } from '@angular/router';
import { ServerService } from '../../service/server.service';
import { BlockchainService } from '../../service/blockchain.service';
import { User } from '../../others/models/user.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'seller.component.html'
})
export class SellerComponent implements OnInit {

  seller: Seller;
  login_name: string;
  password: string;
  constructor(private service: ServerService, private router: Router, private blockchain: BlockchainService) { }

  ngOnInit() {
    this.seller = new Seller();
  }

  onSubmit() {
    CustomLogger.logStringWithObject("seller to be saved: ", this.seller);
    //first create a new user with the given credentials
    let user = new User();
    user.email = this.seller.email;
    user.login_name = this.login_name;
    user.password = this.password;
    user.type = CustomGlobalConstants.CONSTANT_SELLER;


    this.service.saveNewUser(user).subscribe(
      data => {
        CustomLogger.logStringWithObject("User Result from db : ", data);
        this.seller.user_uuid = data["data"].uuid;
        this.service.saveSeller(this.seller).subscribe(
          data2 => {
            this.seller.uuid = data2["data"]["uuid"];
            CustomLogger.logStringWithObject("Seller Result from db : ", this.seller);

            if (Globals.shouldUseBlockchain()) {
              ////////////blockchain - start
              this.blockchain.addSeller(this.seller).subscribe(
                data1 => {
                  CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
                  alert("Seller added to blockchain");
                  this.router.navigate(["ourdashboard"]);
                },
                err1 => {
                  CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
                  this.router.navigate(["ourdashboard"]);
                }
              );
              ////////////blockchain - end
            } else {
              this.router.navigate(["ourdashboard"]);
            }
          },
          err2 => {
            CustomLogger.logStringWithObject("Error from db: ", err2);
          }
        );
      },
      err => {
        CustomLogger.logStringWithObject("Error from db: ", err);
      }
    );
    // this.router.navigate(["ourdashboard"]);
  }
}
