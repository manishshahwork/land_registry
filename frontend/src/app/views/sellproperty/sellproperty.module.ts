import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SellpropertyRoutingModule } from './sellproperty-routing.module';
import { SellpropertyComponent } from './sellproperty.component';


@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    SellpropertyRoutingModule,
    FormsModule
  ],
  declarations: [ SellpropertyComponent ]
})
export class SellpropertyModule { } 
