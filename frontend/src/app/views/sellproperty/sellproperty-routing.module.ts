import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellpropertyComponent } from './sellproperty.component';

const routes: Routes = [
  {
    path: '',
    component: SellpropertyComponent,
    data: {
      title: 'Buyers'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellpropertyRoutingModule {} 
