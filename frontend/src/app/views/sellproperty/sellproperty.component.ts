import { Component, OnInit } from '@angular/core';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SellProperty } from '../../others/models/sellProperty.model';
import { BlockchainService } from '../../service/blockchain.service';
import { Buyproperty } from '../../others/models/buyproperty.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'sellproperty.component.html'
})
export class SellpropertyComponent implements OnInit {
  sellProperty: SellProperty;
  currentBuyProperty: Buyproperty;

  buyerList: any;
  sellerList: any;
  propertyList: any;
  htmlSpace = " ";
  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router, private blockchain: BlockchainService) { }


  ngOnInit() {
    this.sellProperty = new SellProperty();

    this.activatedRoute.params.subscribe(
      params => {
        CustomLogger.logStringWithObject("SellpropertyComponent: Got params.... ", params);
        if (params.buyproperty_uuid) {
          this.service.getBuyPropertyById(params.buyproperty_uuid).subscribe(
            data => {
              CustomLogger.logStringWithObject("getBuyPropertyById: Result ggot : ", data);
              this.currentBuyProperty = data["data"];
              this.sellProperty = data["data"];
            },
            err => {
              CustomLogger.logStringWithObject("getBuyPropertyById: Error got: ", err);
            }
          );
        }
      }
    );
  }


  onSubmit() {

    //change the status of buyproperty to Closed
    this.currentBuyProperty.current_status = CustomGlobalConstants.CONSTANT_CLOSED;

    //get all buyproperties whose seller_uuid and property_uuid matches and delete all of them
    this.currentBuyProperty.is_deleted = true; //currently doing it for one buyproperty
    this.service.updateBuyproperty(this.currentBuyProperty).subscribe(
      data => {
        CustomLogger.logStringWithObject("updateBuyproperty: Result got : ", data);

        ////////////blockchain - start
        this.blockchain.updateBuyProperty(this.currentBuyProperty).subscribe(
          data1 => {
            CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
            // alert("Object updated to blockchain");
            // this.router.navigate(["ourdashboard"]);
          },
          err1 => {
            CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
            // this.router.navigate(["ourdashboard"]);
          }
        );
        ////////////blockchain - end

      },
      err => {
        CustomLogger.logStringWithObject("updateBuyproperty: Result got : ", err);
      }
    );

    CustomLogger.logStringWithObject("sellproperty to be saved: ", this.sellProperty);
    let tmpSellProperty = this.sellProperty;
    this.service.saveSellProperty(this.sellProperty).subscribe(
      data => {
        CustomLogger.logStringWithObject("saveSellProperty: Result got : ", data);

        ////////////blockchain - start
        this.blockchain.addSellProperty(this.sellProperty).subscribe(
          data1 => {
            CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
            // alert("Object updated to blockchain");
            // this.router.navigate(["ourdashboard"]);
          },
          err1 => {
            CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
            // this.router.navigate(["ourdashboard"]);
          }
        );
        ////////////blockchain - end

        //update the property available flag to false
        this.service.getPropertyById(tmpSellProperty.property_uuid).subscribe(
          data5 => {
            CustomLogger.logStringWithObject("getPropertyById: Result got : ", data5);
            let prop = data5["data"];
            prop.available_for_sale = false;
            //change the seller
            prop.seller_uuid = tmpSellProperty.buyer_uuid;
            prop.seller_name = tmpSellProperty.buyer_name;
            this.service.updateProperty(prop).subscribe(
              data6 => {
                CustomLogger.logStringWithObject("updateProperty: Result got : ", data6);

                ////////////blockchain - start
                this.blockchain.updateSellProperty(prop).subscribe(
                  data1 => {
                    CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
                    // alert("Object updated to blockchain");
                    // this.router.navigate(["ourdashboard"]);
                  },
                  err1 => {
                    CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
                    // this.router.navigate(["ourdashboard"]);
                  }
                );
                ////////////blockchain - end

                this.router.navigate(["ourdashboard"]);
              },
              err6 => {
                CustomLogger.logStringWithObject("updateProperty: Error got : ", err6);
              }
            );
          },
          err5 => {
            CustomLogger.logStringWithObject("getPropertyById: Error got : ", err5);
          }
        );

        if (Globals.shouldUseBlockchain()) {
          this.sellProperty.uuid = data["data"]["uuid"];
          this.blockchain.addSellProperty(this.sellProperty).subscribe(
            data10 => {
              CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data10);
              alert("Property Sold and added to blockchain");
              this.router.navigate(["ourdashboard"]);
            },
            err10 => {
              CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err10);
              this.router.navigate(["ourdashboard"]);
            }
          );
        }

      },
      err => {
        CustomLogger.logStringWithObject("saveSellProperty Error got: ", err);
      }
    );

  }
}
