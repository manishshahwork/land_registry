import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuypropertyComponent } from './buyproperty.component';

const routes: Routes = [
  {
    path: '',
    component: BuypropertyComponent,
    data: {
      title: 'Buyers'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuypropertyRoutingModule {} 
