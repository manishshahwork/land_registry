import { Component, OnInit } from '@angular/core';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockchainService } from '../../service/blockchain.service';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Buyproperty } from '../../others/models/buyproperty.model';
import { Property } from '../../others/models/property.model';
import { CustomMisc } from '../../others/utils/CustomMisc';

@Component({
  templateUrl: 'buyproperty.component.html'
})
export class BuypropertyComponent implements OnInit {
  buyproperty: Buyproperty;
  currentProperty: Property;
  seller_price = "";
  propertyList: any;
  htmlSpace = " ";

  isSellerAccessingFlag = false;
  htmlPropertyStatusArr = [];

  constructor(private service: ServerService, private router: Router, private activatedRoute: ActivatedRoute, private blockchain: BlockchainService) { }

  ngOnInit() {
    this.buyproperty = new Buyproperty();
    this.currentProperty = new Property();

    //check for params
    this.activatedRoute.params.subscribe(
      params => {
        CustomLogger.logStringWithObject("params: ", params);
        if (params != null && params.buyproperty_uuid) {
          this.isSellerAccessingFlag = true;
          this.htmlPropertyStatusArr = CustomGlobalConstants.PROPERTY_STATUS;
          this.service.getBuyPropertyById(params.buyproperty_uuid).subscribe(
            data => {
              CustomLogger.logStringWithObject("Buyproperty result got : ", data);
              this.buyproperty = data["data"];
              this.fillCurrentProperty(this.buyproperty.property_uuid);
            },
            err => {
              CustomLogger.logStringWithObject("Error got: ", err);
            }
          );

        }
      }
    );



    //DB
    // this.service.getAllProperties().subscribe(
    //get all properties which are available for sale
    this.service.getSpecificProperties(CustomGlobalConstants.CONSTANT_REALTOR, null).subscribe(
      data => {
        CustomLogger.logStringWithObject("Result ggot : ", data);
        this.propertyList = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("Error got: ", err);
      }
    );

    //BLOCKCHAIN
    // this.blockchain.getPropertiesList().subscribe(
    //   data => {
    //     CustomLogger.logStringWithObject("Result ggot : ", data);
    //     this.propertyList = data;
    //   },
    //   err => {
    //     CustomLogger.logStringWithObject("Error got: ", err);
    //   }
    // );
  }

  getPropertyNameFromUuid(uuid) {
    let name = "Dummy";
    for (let k = 0; k < this.propertyList.length; k++) {
      if (this.propertyList[k].uuid == uuid) {
        name = (this.propertyList[k].property_address != "" && this.propertyList[k].property_address != null && this.propertyList[k].property_address != undefined) ? this.propertyList[k].property_address : "";
        break;
      }
    }
    console.log("nameeeee::: " + name);
    return name;
  }


  async onSubmit() {

    if (this.uploadFlag) {
      //temporary object to hold values
      let tmpBuyProperty = this.buyproperty;
      let tmpService = this.service;
      await CustomMisc.uploadFileToRemoteServer(this.currentFile).then(function (dataLocation) {
        CustomLogger.logString("gottttttttttttt");
        CustomLogger.logObj(dataLocation);
        CustomLogger.logString("tmpBuyProperty");
        CustomLogger.logObj(tmpBuyProperty);
        CustomLogger.logStringWithObject("CURRENT_UPLOAD_FILE_NAME:", localStorage.getItem("CURRENT_UPLOAD_FILE_NAME"));
        //CustomLogger.logString("this.incentive");
        //CustomLogger.logObj(this.incentive);
        //this.incentive.image_link = dataLocation;
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
        tmpBuyProperty.agreement_document_url = dataLocation.toString();
        tmpBuyProperty.agreement_document_name = localStorage.getItem("CURRENT_UPLOAD_FILE_NAME");

        // //update property with agreement details
        // tmpService.getPropertyById(tmpBuyProperty.property_uuid).subscribe(
        //   data1 => {
        //     CustomLogger.logStringWithObject("getPropertyById Result gott : ", data1);
        //     let property = data1["data"];
        //     property.agreement_document_name = localStorage.getItem("CURRENT_UPLOAD_FILE_NAME");
        //     property.agreement_document_url = dataLocation.toString();
        //     tmpService.updateProperty(property).subscribe(
        //       data2=>{
        //         CustomLogger.logStringWithObject("updateProperty Result gott : ", data2);
        //       },
        //       err2=>{
        //         CustomLogger.logStringWithObject("updateProperty Error got : ", err2);
        //       }
        //     );

        //   },
        //   err1 => {
        //     CustomLogger.logStringWithObject("getPropertyById Error got : ", err1);

        //   }
        // );

      });
      //convert back to original object
      this.buyproperty = tmpBuyProperty;
      CustomLogger.logString("this.buyproperty");
      CustomLogger.logObj(this.buyproperty);

    }


    // this.buyproperty.property_name = this.getPropertyNameFromUuid(this.buyproperty.property_uuid);
    if (!this.isSellerAccessingFlag) {
      this.buyproperty.current_status = CustomGlobalConstants.CONSTANT_REQUEST;
      this.buyproperty.property_uuid = this.currentProperty.uuid;
      this.buyproperty.property_name = this.currentProperty.property_name;
      this.buyproperty.seller_uuid = this.currentProperty.seller_uuid;
      this.buyproperty.seller_name = this.currentProperty.seller_name;
      this.buyproperty.buyer_uuid = Globals.getUserTypeId();

      this.service.getSingleBuyer(this.buyproperty.buyer_uuid).subscribe(
        data1 => {
          CustomLogger.logStringWithObject("getSingleBuyer Result gott : ", data1);
          this.buyproperty.buyer_name = data1["data"].first_name + " " + data1["data"].last_name;
          CustomLogger.logStringWithObject("buyproperty to be saved: ", this.buyproperty);
          this.service.saveBuyproperty(this.buyproperty).subscribe(
            data => {
              CustomLogger.logStringWithObject("saveBuyproperty Result gott : ", data);
              
              if (Globals.shouldUseBlockchain()) {
                ////////////blockchain - start
                this.blockchain.addBuyProperty(this.buyproperty).subscribe(
                  data1 => {
                    CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
                    alert("Added to blockchain");
                    this.router.navigate(["ourdashboard"]);
                  },
                  err1 => {
                    CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
                    this.router.navigate(["ourdashboard"]);
                  }
                );
                ////////////blockchain - end
              }
              this.router.navigate(["ourdashboard"]);
            },
            err => {
              CustomLogger.logStringWithObject("Error got: ", err);
              this.router.navigate(["ourdashboard"]);
            }
          );
        },
        err1 => {
          CustomLogger.logStringWithObject("getSingleBuyer Error gott : ", err1);
          this.router.navigate(["ourdashboard"]);
        }
      );


    } else {
      this.service.updateBuyproperty(this.buyproperty).subscribe(
        data => {
          CustomLogger.logStringWithObject("updateBuyproperty Result gott : ", data);
          
          // ////////////blockchain - start
          // this.blockchain.updateBuyProperty(this.buyproperty).subscribe(
          //   data1 => {
          //     CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
          //     alert("Added to blockchain");
          //     this.router.navigate(["ourdashboard"]);
          //   },
          //   err1 => {
          //     CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
          //     this.router.navigate(["ourdashboard"]);
          //   }
          // );
          // ////////////blockchain - end
          this.router.navigate(["ourdashboard"]);
        },
        err => {
          CustomLogger.logStringWithObject("Error got: ", err);
          this.router.navigate(["ourdashboard"]);
        }
      );
    }
  }

  fillCurrentProperty(property_uuid) {
    CustomLogger.logStringWithObject("About to get property details for : ", property_uuid);
    this.service.getPropertyById(property_uuid).subscribe(
      data => {
        CustomLogger.logStringWithObject("got property data: ", data);
        this.currentProperty = data["data"];
        // this.seller_price = data["data"].property_price;
      },
      err => {
        CustomLogger.logStringWithObject("got error: ", err);
      }
    );
  }

  onSelectProperty(event) {
    CustomLogger.logStringWithObject("Respective property will be retrieved...", event.target.value);
    this.fillCurrentProperty(event.target.value);
  }

  currentFile: File;
  CURRENT_UPLOAD_FILE_NAME = "CURRENT_UPLOAD_FILE_NAME";
  uploadFlag: boolean = false;
  htmlImageLink: string = CustomGlobalConstants.DEFAULT_IMAGE_FILE_LINK;
  onFileUpload(event) {
    CustomLogger.logStringWithObject("Respective document will be uploaded...", event.target.value);

    //let file: File = files.item(0);
    let file = event.target.files[0];
    this.currentFile = file;
    localStorage.setItem(this.CURRENT_UPLOAD_FILE_NAME, this.currentFile.name);
    ////this.uploadFile(file);
    CustomLogger.logString("will upload file::: ");
    CustomLogger.logObj(this.currentFile);
    this.uploadFlag = true; // flag required in order to update file on server
    // this.htmlUploadedName = this.currentFile.name;

    //preview the file
    var reader = new FileReader();
    reader.readAsDataURL(file); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      // console.log("inside onload event...");
      this.htmlImageLink = event.target.result;
      // console.log("htmlImageLink:::' " + this.htmlImageLink);
    }

  }

}//end of class
