import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BuypropertyRoutingModule } from './buyproperty-routing.module';
import { BuypropertyComponent } from './buyproperty.component';

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    BuypropertyRoutingModule,
    FormsModule
  ],
  declarations: [ BuypropertyComponent ]
})
export class BuypropertyModule { }  
