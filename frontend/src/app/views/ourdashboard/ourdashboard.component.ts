import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { ServerService } from '../../service/server.service';
import { Globals } from '../../others/models/Globals';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { BlockchainService } from '../../service/blockchain.service';

@Component({
	templateUrl: 'ourdashboard.component.html',
	styleUrls: ['ourdashboard.component.css']
})
export class OurdashboardComponent implements OnInit {

	dashboardHeading = "Current Property Related Chart !";
	userType: any;

	//show spinner flag
	showSpinnerFlag: boolean = false;

	//buyer specific
	buyer_totalPropertiesForSale: any;
	buyer_totalPropertiesRequested: any;
	buyer_totalPropertiesBought: any;
	buyer_showBuyerFlag = false;

	//seller specific
	seller_totalProperties: any;
	seller_totalPropertiesSold: any;
	seller_showSellerFlag = false;

	//realtor specific
	totalBuyers: any;
	totalSellers: any;
	totalProperties: any;
	totalSellProperties: any;

	//land registry specific
	landRegistry_totalPropertiesForApproval: any;
	landRegistry_totalPropertiesApproved: any;
	landRegistry_showLandRegistryFlag = false;

	//
	searchStartTimeString: any;
	searchEndTimeString: any;

	LineChart1 = [];
	LineChart = [];
	barChart2 = [];
	barChart1 = [];
	startDate: any;
	endDate: any;
	chartData = [];
	arryaData = [];
	arryaData1 = [];


	//////
	ChartRewardSpecificDetails = [];
	allRewardsSpecificDetailsObj: any;
	ChartCouponSpecificDetails = [];
	allCouponsSpecificDetailsObj: any;

	constructor(private _serverService: ServerService, private blockchain: BlockchainService, private router: Router) {
	}

	async ngOnInit() {
		//////////
		this.userType = Globals.getUserType();
		if (this.userType == CustomGlobalConstants.CONSTANT_BUYER) {
			this.buyer_showBuyerFlag = true;
			this.buyer_totalPropertiesForSale = 10;
			this._serverService.getPropertiesAvailableForSale().subscribe(
				data1 => {
					CustomLogger.logStringWithObject("getPropertiesAvailableForSale Result got : ", data1);
					this.buyer_totalPropertiesForSale = data1["data"].length;
					this._serverService.getBuyersBiddedProperties(Globals.getUserTypeId()).subscribe(
						data2 => {
							CustomLogger.logStringWithObject("getBuyersBiddedProperties Result got : ", data2);
							CustomLogger.logStringWithObject("getBuyersBiddedProperties data length : ", data2["data"].length);
							this.buyer_totalPropertiesRequested = data2["data"].length;

							this._serverService.getSoldProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
								data3 => {
									CustomLogger.logStringWithObject("getSoldProperties: Result got : ", data3);
									//   CustomLogger.logStringWithObject("getSoldProperties: Result data length : ", data3["data"].length);
									this.buyer_totalPropertiesBought = data3["data"].length;
									this.drawBuyerChart();
								},
								err3 => {
									CustomLogger.logStringWithObject("getSoldProperties: Error got : ", err3);
								}
							);

						},
						err2 => {
							CustomLogger.logStringWithObject("getBuyersBiddedProperties Error got: ", err2);
						}
					);
				},
				err1 => {
					CustomLogger.logStringWithObject("getPropertiesAvailableForSale Error got: ", err1);
				}
			);
		}

		else if (this.userType == CustomGlobalConstants.CONSTANT_SELLER) {
			this.seller_showSellerFlag = true;
			this.seller_totalProperties = 5;
			this.seller_totalPropertiesSold = 3;
			this._serverService.getSpecificProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
				data1 => {
					CustomLogger.logStringWithObject("getPropertiesAvailableForSale Result got : ", data1);
					this.seller_totalProperties = data1["data"].length;
					this._serverService.getSoldPropertiesOfSeller(Globals.getUserTypeId()).subscribe(
						data2 => {
							CustomLogger.logStringWithObject("getSoldPropertiesOfSeller Result got : ", data2);

							let k = data2["data"];
							CustomLogger.logStringWithObject("k::", k);
							CustomLogger.logStringWithObject("k.length::", k.length);

							CustomLogger.logStringWithObject("getSoldPropertiesOfSeller data length : ", data2["data"].length);
							this.seller_totalPropertiesSold = data2["data"].length;
							this.drawSellerChart();
						},
						err2 => {
							CustomLogger.logStringWithObject("getSoldPropertiesOfSeller Error got: ", err2);
						}
					);
				},
				err1 => {
					CustomLogger.logStringWithObject("getPropertiesAvailableForSale Error got: ", err1);
				}
			);
		}

		else if (this.userType == CustomGlobalConstants.CONSTANT_REALTOR) {
			// this.blockchain.getBuyersList().subscribe(
			this._serverService.getAllBuyers().subscribe(
				data => {
					CustomLogger.logStringWithObject("getAllBuyers Result got : ", data);
					this.totalBuyers = data["data"].length;
					CustomLogger.logString("Total Buyers: " + this.totalBuyers);
					// this.blockchain.getSellersList().subscribe(
					this._serverService.getAllSellers().subscribe(
						data => {
							CustomLogger.logStringWithObject("Result got : ", data);
							this.totalSellers = data["data"].length;
						},
						err => {
							CustomLogger.logStringWithObject("Error got: ", err);
						}
					);
					// this.blockchain.getPropertiesList().subscribe(
					this._serverService.getPropertiesAvailableForSale().subscribe(
						data => {
							CustomLogger.logStringWithObject("Result got : ", data);
							this.totalProperties = data["data"].length;
						},
						err => {
							CustomLogger.logStringWithObject("Error got: ", err);
						}
					);
					// this.blockchain.getSellPropertiesList().subscribe(
					this._serverService.getAllSellProperties().subscribe(
						data => {
							CustomLogger.logStringWithObject("Result got : ", data);
							this.totalSellProperties = data["data"].length;
							this.drawRealtorChart();
						},
						err => {
							CustomLogger.logStringWithObject("Error got: ", err);
						}
					);
				},
				err => {
					CustomLogger.logStringWithObject("Error got: ", err);
				}
			);
		}

		else if (this.userType == CustomGlobalConstants.CONSTANT_LAND_REGISTRY) {
			this.landRegistry_showLandRegistryFlag = true;
			this.landRegistry_totalPropertiesApproved = 5;
			this.landRegistry_totalPropertiesForApproval = 3;
			this._serverService.getLandRegistrySpecificPropertiesCount(true).subscribe(
				data1 => {
					CustomLogger.logStringWithObject("getLandRegistrySpecificPropertiesCount1 Result got : ", data1);
					this.landRegistry_totalPropertiesApproved = data1["data"].length;
					this._serverService.getLandRegistrySpecificPropertiesCount(false).subscribe(
						data2 => { 
							CustomLogger.logStringWithObject("getLandRegistrySpecificPropertiesCount2 Result got : ", data2);
							this.landRegistry_totalPropertiesForApproval = data2["data"].length;
							this.drawLandRegistryChart();
						},
						err2 => { 
							CustomLogger.logStringWithObject("getLandRegistrySpecificPropertiesCount2 Error got: ", err2);
						}
					);
				},
				err1 => { 
					CustomLogger.logStringWithObject("getLandRegistrySpecificPropertiesCount1 Error got: ", err1);
				}
			);

			// this._serverService.getSpecificProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
			// 	data1 => {
			// 		CustomLogger.logStringWithObject("getPropertiesAvailableForSale Result got : ", data1);
			// 		this.seller_totalProperties = data1["data"].length;
			// 		this._serverService.getSoldPropertiesOfSeller(Globals.getUserTypeId()).subscribe(
			// 			data2 => {
			// 				CustomLogger.logStringWithObject("getSoldPropertiesOfSeller Result got : ", data2);

			// 				let k = data2["data"];
			// 				CustomLogger.logStringWithObject("k::", k);
			// 				CustomLogger.logStringWithObject("k.length::", k.length);

			// 				CustomLogger.logStringWithObject("getSoldPropertiesOfSeller data length : ", data2["data"].length);
			// 				this.seller_totalPropertiesSold = data2["data"].length;
			// 				this.drawSellerChart();
			// 			},
			// 			err2 => {
			// 				CustomLogger.logStringWithObject("getSoldPropertiesOfSeller Error got: ", err2);
			// 			}
			// 		);
			// 	},
			// 	err1 => {
			// 		CustomLogger.logStringWithObject("getPropertiesAvailableForSale Error got: ", err1);
			// 	}
			// );
		}

	}

	drawBuyerChart() {
		//chart
		var xAxisLabels = [
			'Total Properties for Sale',
			'Total Properties Bought',
			'Total Requested Properties'
		];
		var yAxisData = [
			this.buyer_totalPropertiesForSale,
			this.buyer_totalPropertiesBought,
			this.buyer_totalPropertiesRequested
		];
		this.ChartRewardSpecificDetails = new Chart("ChartRewardSpecificDetails", {
			type: 'bar',
			data: {
				labels: xAxisLabels,
				datasets: [{
					label: 'Details',
					data: yAxisData,
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				title: {
					display: false,
					text: 'Property Details'
				},
				legend: {
					display: false,
					labels: {
						fontColor: 'rgb(255, 99, 132)'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	drawSellerChart() {
		//chart
		var xAxisLabels = [
			'Total Properties for Sale',
			'Toal Properties Sold'
		];
		var yAxisData = [
			this.seller_totalProperties,
			this.seller_totalPropertiesSold
		];
		this.ChartRewardSpecificDetails = new Chart("ChartRewardSpecificDetails", {
			type: 'bar',
			data: {
				labels: xAxisLabels,
				datasets: [{
					label: 'Details',
					data: yAxisData,
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				title: {
					display: false,
					text: 'Property Details'
				},
				legend: {
					display: false,
					labels: {
						fontColor: 'rgb(255, 99, 132)'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	drawRealtorChart() {
		//chart
		var xAxisLabels = [
			'Buyers',
			'Sellers',
			'Properties To Sell',
			'Sold Properties'
		];
		var yAxisData = [
			this.totalBuyers,
			this.totalSellers,
			this.totalProperties,
			this.totalSellProperties
		];
		this.ChartRewardSpecificDetails = new Chart("ChartRewardSpecificDetails", {
			type: 'bar',
			data: {
				labels: xAxisLabels,
				datasets: [{
					label: 'Details',
					data: yAxisData,
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				title: {
					display: false,
					text: 'Property Details'
				},
				legend: {
					display: false,
					labels: {
						fontColor: 'rgb(255, 99, 132)'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}


	drawLandRegistryChart() {
		//chart
		var xAxisLabels = [
			'Properties require Approval',
			'Properties Approved'
		];
		var yAxisData = [
			this.landRegistry_totalPropertiesForApproval,
			this.landRegistry_totalPropertiesApproved
		];
		this.ChartRewardSpecificDetails = new Chart("ChartRewardSpecificDetails", {
			type: 'bar',
			data: {
				labels: xAxisLabels,
				datasets: [{
					label: 'Details',
					data: yAxisData,
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				title: {
					display: false,
					text: 'Property Details'
				},
				legend: {
					display: false,
					labels: {
						fontColor: 'rgb(255, 99, 132)'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}


	onClickTest() {
		this._serverService.showMessage("111111111111");
	}
}
