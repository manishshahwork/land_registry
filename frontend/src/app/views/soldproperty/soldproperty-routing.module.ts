import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SoldpropertyComponent } from './soldproperty.component';


const routes: Routes = [
  {
    path: '',
    component: SoldpropertyComponent,
    data: {
      title: 'Buyers'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoldpropertyRoutingModule {} 
