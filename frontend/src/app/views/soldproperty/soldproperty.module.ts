import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SoldpropertyComponent } from './soldproperty.component';
import { SoldpropertyRoutingModule } from './soldproperty-routing.module';
import { DataTableModule } from 'angular-6-datatable';

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    SoldpropertyRoutingModule,
    FormsModule,
    DataTableModule
  ],
  declarations: [ SoldpropertyComponent ]
})
export class SoldpropertyModule { } 
