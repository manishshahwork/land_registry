import { Component, OnInit } from '@angular/core';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SellProperty } from '../../others/models/sellProperty.model';
import { BlockchainService } from '../../service/blockchain.service';
import { Buyproperty } from '../../others/models/buyproperty.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'soldproperty.component.html'
})
export class SoldpropertyComponent implements OnInit {
  
  tableDataArr: any;
  htmlSpace = " ";
  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router, private blockchain: BlockchainService) { }


  ngOnInit() {
    this.service.getSoldProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
      data => { 
        CustomLogger.logStringWithObject("getSoldProperties: Result got : ", data);
        this.tableDataArr = data["data"];
      },
      err => { 
        CustomLogger.logStringWithObject("getSoldProperties: Error got : ", err);
      }
    );
  }

  onClickView(event){
    CustomLogger.logString("The following property will be viewed: " + event.target.value);
    this.router.navigate(["property", event.target.value]);
  }
  
}
