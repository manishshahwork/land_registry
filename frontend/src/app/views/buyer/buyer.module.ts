import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { BuyerComponent } from './buyer.component';
import { BuyerRoutingModule } from './buyer-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    BuyerRoutingModule,
    FormsModule
  ],
  declarations: [ BuyerComponent ]
})
export class BuyerModule { } 
