import { Component, OnInit } from '@angular/core';
import { Buyer } from '../../others/models/buyer.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router } from '@angular/router';
import { BlockchainService } from '../../service/blockchain.service';
import { User } from '../../others/models/user.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'buyer.component.html'
})
export class BuyerComponent implements OnInit {
  buyer: Buyer;
  login_name: string;
  password: string;
  constructor(private service: ServerService, private router: Router, private blockchain: BlockchainService) { }

  ngOnInit() {
    this.buyer = new Buyer();
  }

  onSubmit() {
    CustomLogger.logStringWithObject("Buyer to be saved: ", this.buyer);
    //first create a new user with the given credentials
    let user = new User();
    user.email = this.buyer.email;
    user.login_name = this.login_name;
    user.password = this.password;
    user.type = CustomGlobalConstants.CONSTANT_BUYER;

    this.service.saveNewUser(user).subscribe(
      data => {
        CustomLogger.logStringWithObject("User Result from db : ", data);
        this.buyer.user_uuid = data["data"].uuid;
        this.service.saveBuyer(this.buyer).subscribe(
          data2 => {
            this.buyer.uuid = data2["data"]["uuid"];
            CustomLogger.logStringWithObject("Result from db : ", this.buyer);

            if (Globals.shouldUseBlockchain()) {
              ////////////blockchain - start
              this.blockchain.addBuyer(this.buyer).subscribe(
                data1 => {
                  CustomLogger.logStringWithObject("Result from BLOCKCHAIN : ", data1);
                  alert("Buyer added to blockchain");
                  this.router.navigate(["ourdashboard"]);
                },
                err1 => {
                  CustomLogger.logStringWithObject("Error from BLOCKCHAIN: ", err1);
                  this.router.navigate(["ourdashboard"]);
                }
              );
              ////////////blockchain - end
            } else {
              this.router.navigate(["ourdashboard"]);
            }
          },
          err2 => {
            CustomLogger.logStringWithObject("Error from db: ", err2);
          }
        );
      },
      err => {
        CustomLogger.logStringWithObject("Error from db: ", err);
      }
    );
    // this.router.navigate(["ourdashboard"]);
  }
}
