import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule, TabsModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';  
import { ApprovepropertyRoutingModule } from './approveproperty-routing.module';
import { ApprovepropertyComponent } from './approveproperty.component';

@NgModule({
  imports: [
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    ApprovepropertyRoutingModule,
    FormsModule,
    TabsModule,
    LeafletModule.forRoot()
  ],
  declarations: [ ApprovepropertyComponent ]
})
export class ApprovepropertyModule { } 
