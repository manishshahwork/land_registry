import { Component, OnInit } from '@angular/core';
import { Property } from '../../others/models/property.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockchainService } from '../../service/blockchain.service';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { latLng, tileLayer } from 'leaflet';
import * as L from 'leaflet';
import { Observable } from 'rxjs';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { Buyproperty } from '../../others/models/buyproperty.model';

@Component({
  templateUrl: 'approveproperty.component.html'
})
export class ApprovepropertyComponent implements OnInit {

  buyproperty: Buyproperty;
  property: Property;
  isDocumentApprovedFlag: boolean;

  htmlSpace = " ";
  sellerList: any;
  // isBuyerAccessingFlag = false;
  isEditableFlag = true;
  showDisplaySellerList = true;
  isNewPropertyFlag = true;

  //file related
  htmlUploadedImageName: string = "";
  htmlImageLink: string = CustomGlobalConstants.DEFAULT_IMAGE_FILE_LINK;
  currentImageFile: File;
  uploadImageFlag: boolean = false;


  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router, private blockchain: BlockchainService) { }

  ngOnInit() {
    this.buyproperty = new Buyproperty();
    this.property = new Property();

    //get buyproperty uuid
    this.activatedRoute.params.subscribe(
      params => {
        CustomLogger.logStringWithObject("Got params : ", params);
        let buyproperty_uuid = params.buyproperty_uuid;
        if (buyproperty_uuid) {
          this.service.getBuyPropertyById(buyproperty_uuid).subscribe(
            data => {
              CustomLogger.logStringWithObject("getBuyPropertyById Result  from db: ", data);
              this.buyproperty = data["data"];

              //now get property details
              this.service.getPropertyById(this.buyproperty.property_uuid).subscribe(
                data1 => {
                  CustomLogger.logStringWithObject("getBuyPropertyById Result  from db: ", data1);
                  this.property = data1["data"];
                },
                err1 => {
                  CustomLogger.logStringWithObject("getPropertyById Error from db: ", err1);
                }
              );

            },
            err => {
              CustomLogger.logStringWithObject("getPropertyById Error from db: ", err);
            }
          );
        }
      }
    );
  }

  async onSubmit() {

    CustomLogger.logStringWithObject("Buy Property to be saved:", this.buyproperty);
    CustomLogger.logStringWithObject("Property to be saved:", this.property);


    //if the document is approved then change the status of document accordingly
    if (this.buyproperty.is_document_approved) {
      this.buyproperty.current_status = CustomGlobalConstants.CONSTANT_APPROVED;
    }

    this.service.updateProperty(this.property).subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from updateProperty: ", data);
        //update the buyproperty status
        this.service.updateBuyproperty(this.buyproperty).subscribe(
          data1 => { 
            CustomLogger.logStringWithObject("Result from updateBuyproperty: ", data1);
            this.router.navigate["ourdashboard"];
          },
          err1 => { 
            CustomLogger.logStringWithObject("Error from updateBuyproperty: ", err1);
            this.router.navigate["ourdashboard"];
          }
        );
      },
      err => {
        CustomLogger.logStringWithObject("Error from updateProperty: ", err);
      }
    );
  }


  async onImageUpload(event) {
    //let file: File = files.item(0);
    let imageFile = event.target.files[0];
    this.currentImageFile = imageFile;
    ////this.uploadImageFile(file);
    CustomLogger.logString("will upload file::: ");
    CustomLogger.logObj(this.currentImageFile);
    this.uploadImageFlag = true; // flag required in order to update image on server
    this.htmlUploadedImageName = this.currentImageFile.name;

    //preview the image

    var reader = new FileReader();
    reader.readAsDataURL(imageFile); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      // console.log("inside onload event...");
      this.htmlImageLink = event.target.result;
      // console.log("htmlImageLink:::' " + this.htmlImageLink);
    }
  }

  updateHTMLSpecificFields() {
    //update html image link
    this.htmlImageLink = this.property.document_location_url;
    // console.log("start time:::: ");
    //var htmlStartTime = this.getTime(this.incentive.start_time);
    // console.log(this.htmlStartTime);
    // this.htmlDateRange = this.htmlStartTime + " - " + this.htmlEndTime;

  }

}
