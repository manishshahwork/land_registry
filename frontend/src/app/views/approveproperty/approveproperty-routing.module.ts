import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Property } from '../../others/models/property.model';
import { ApprovepropertyComponent } from './approveproperty.component';

const routes: Routes = [
  {
    path: '',
    component: ApprovepropertyComponent,
    data: {
      title: 'Properties'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovepropertyRoutingModule{

 
} 
 