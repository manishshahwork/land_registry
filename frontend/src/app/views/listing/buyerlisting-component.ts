import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { BlockchainService } from '../../service/blockchain.service';
import { Globals } from '../../others/models/Globals';


@Component({
  templateUrl: 'buyerlisting-component.html'
})
export class BuyerlistingComponent implements OnInit {

  tableDataArr: any;

  constructor(private service: ServerService, private blockchain: BlockchainService) {
  }

  ngOnInit() {

    this.getAllBuyersFromDB();

    // if (Globals.shouldUseBlockchain())
    //   this.getAllBuyersFromBlockchain();
    // else
    //   this.getAllBuyersFromDB();
  }

  getAllBuyersFromDB() {
    this.service.getAllBuyers().subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from db: ", data);
        this.tableDataArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("Erro from db: ", err);
      }
    );
  }

  getAllBuyersFromBlockchain() {
    this.blockchain.getBuyersList().subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from BLOCKCHAIN: ", data);
        this.tableDataArr = data;
      },
      err => {
        CustomLogger.logStringWithObject("Erro from BLOCKCHAIN: ", err);
      }
    );
  }

  modifyTableData(event) {
    CustomLogger.logStringWithObject("Modify table data..", event);
  }


  onClickDelete(event) {
    CustomLogger.logStringWithObject("Delete table data..", event);
  }


}
