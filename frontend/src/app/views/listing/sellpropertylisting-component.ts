

import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Property } from '../../others/models/property.model';
import { BlockchainService } from '../../service/blockchain.service';
import { SellProperty } from '../../others/models/sellProperty.model';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'sellpropertylisting-component.html'
})
export class SellpropertylistingComponent {
  tableDataArr: any;
  sellPropertiesList: Array<SellProperty> = [];
  constructor(private service: ServerService, private blockchain: BlockchainService) {
  }

  ngOnInit() {
    this.getAllSellPropertiesFromDB();
    // if (Globals.shouldUseBlockchain())
    //   this.getAllSellPropertiesFromBlockchain();
    // else
    //   this.getAllSellPropertiesFromDB();
  }

  getAllSellPropertiesFromBlockchain() {
    this.blockchain.getSellPropertiesList().subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from BLOCKCHAIN: ", data);
        this.tableDataArr = data;
      },
      err => {
        CustomLogger.logStringWithObject("Erro from BLOCKCHAIN: ", err);
      }
    );

  }


  async getAllSellPropertiesFromDB() {
    this.service.getAllSellProperties().subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from dddddb: ", data);

        let arrFromDB = data["data"];

        for (let k = 0; k < arrFromDB.length; k++) {
          let prop = new SellProperty();
          prop.property_uuid = arrFromDB[k].property_uuid;
          prop.buyer_uuid = arrFromDB[k].buyer_uuid;
          prop.seller_uuid = arrFromDB[k].seller_uuid;

          CustomLogger.logStringWithObject("Pinging for property id: ", prop);
          this.service.getPropertyById(prop.property_uuid).subscribe(
            data1 => {
              CustomLogger.logStringWithObject("Result from db1: ", data1);
              prop.property_name = data1["data"].property_address;
            },
            err1 => {
              CustomLogger.logStringWithObject("Erro from db1: ", err1);
            }
          );

          CustomLogger.logStringWithObject("Pinging for buyer id: ", prop);
          this.service.getSingleBuyer(prop.buyer_uuid).subscribe(
            data2 => {
              CustomLogger.logStringWithObject("Result from db1: ", data2);
              prop.buyer_name = data2["data"].first_name + " " + data2["data"].last_name;
            },
            err2 => {
              CustomLogger.logStringWithObject("Erro from db1: ", err2);
            }
          );

          CustomLogger.logStringWithObject("Pinging for seller id: ", prop);
          this.service.getSingleSeller(prop.seller_uuid).subscribe(
            data3 => {
              CustomLogger.logStringWithObject("Result from db1: ", data3);
              prop.seller_name = data3["data"].first_name + " " + data3["data"].last_name;
            },
            err3 => {
              CustomLogger.logStringWithObject("Erro from db1: ", err3);
            }
          );

          this.sellPropertiesList.push(prop);


        }
        CustomLogger.logStringWithObject("llst", this.sellPropertiesList);
        this.tableDataArr = this.sellPropertiesList;

      },
      err => {
        CustomLogger.logStringWithObject("Error from db: ", err);
      }
    );
  }


  modifyTableData(event) {
    CustomLogger.logStringWithObject("Modify table data..", event);
  }


  onClickDelete(event) {
    CustomLogger.logStringWithObject("Delete table data..", event);
  }

}
