// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';
// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap';
import { ListingRoutingModule } from './listing-routing.module';
import { PropertylistingComponent } from './propertylisting-component';
import { DataTableModule } from "angular-6-datatable";
import { InlineEditorModule } from '@qontu/ngx-inline-editor';
import { BuyerlistingComponent } from './buyerlisting-component';
import { SellerlistingComponent } from './sellerlisting-component';
import { SellpropertylistingComponent } from './sellpropertylisting-component';
import { BuypropertylistingComponent } from './buypropertylisting-component.';



@NgModule({
  imports: [
    CommonModule,
    ListingRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    TabsModule,
    DataTableModule,
    InlineEditorModule
  ],
  declarations: [
    SellpropertylistingComponent,
    PropertylistingComponent,
    BuyerlistingComponent,
    SellerlistingComponent,
    BuypropertylistingComponent
  ]
})
export class ListingModule { } 
