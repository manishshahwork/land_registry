import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { BlockchainService } from '../../service/blockchain.service';
import { Router } from '@angular/router';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';

@Component({
  templateUrl: 'propertylisting-component.html'
})
export class PropertylistingComponent implements OnInit {

  tableDataArr: any;
  showActionFlag = true;
  showSellerName = false;
  constructor(private service: ServerService, private router: Router, private blockchain: BlockchainService) {
  }

  ngOnInit() {
    this.getAllPropertiesFromDB();
    // this.getAllPropertiesFromBlockchain();
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_BUYER)
      this.showActionFlag = false;
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_REALTOR)
      this.showSellerName = true;

  }

  getAllPropertiesFromDB() {
    //based on user type get specific properties
    if (Globals.getUserType() == CustomGlobalConstants.CONSTANT_SELLER) {
      this.service.getSpecificProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
        data => {
          CustomLogger.logStringWithObject("getSpecificProperties Result from db: ", data);
          this.tableDataArr = data["data"];
        },
        err => {
          CustomLogger.logStringWithObject("getSpecificProperties Error from db: ", err);
        }
      );
    } else {
      // this.service.getSpecificProperties(Globals.getUserType(), Globals.getUserTypeId()).subscribe(
      //get all properties which are available for sale
      this.service.getPropertiesAvailableForSale().subscribe(
        data => {
          CustomLogger.logStringWithObject("getPropertiesAvailableForSale Result from db: ", data);
          this.tableDataArr = data["data"];
        },
        err => {
          CustomLogger.logStringWithObject("getPropertiesAvailableForSale Error from db: ", err);
        }
      );
    }
  }

  getAllPropertiesFromBlockchain() {
    this.blockchain.getPropertiesList().subscribe(
      data => {
        CustomLogger.logStringWithObject("Result from BLOCKCHAIN: ", data);
        this.tableDataArr = data;
      },
      err => {
        CustomLogger.logStringWithObject("Erro rfrom BLOCKCHAIN: ", err);
      }
    );
  }

  modifyTableData(event) {
    CustomLogger.logStringWithObject("Modify table data..", event);
  }


  onClickEdit(event) {
    CustomLogger.logStringWithObject("Edit Property with id..", event.target.value);
    let property_uuid = event.target.value;
    this.router.navigate(["property", property_uuid]);
  }
}
