import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertylistingComponent } from './propertylisting-component';
import { BuyerlistingComponent } from './buyerlisting-component';
import { SellerlistingComponent } from './sellerlisting-component';
import { SellpropertylistingComponent } from './sellpropertylisting-component';
import { BuypropertylistingComponent } from './buypropertylisting-component.';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Listing'
    },
    children: [
      {
        path: '',
        redirectTo: 'propertylisting'
      },
      {
        path: 'propertylisting',
        component: PropertylistingComponent,
        data: {
          title: 'Listing of Properties'
        }
      },
      {
        path: 'buyerlisting',
        component: BuyerlistingComponent,
        data: {
          title: 'Listing of Buyers'
        }
      },
      {
        path: 'sellerlisting',
        component: SellerlistingComponent,
        data: {
          title: 'Listing of Sellers'
        }
      },
      {
        path: 'sellpropertylisting',
        component: SellpropertylistingComponent,
        data: {
          title: 'Listing of Sold Properties'
        }
      },
      {
        path: 'buypropertylisting',
        component: BuypropertylistingComponent,
        data: {
          title: 'Property Reports Listing'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingRoutingModule {}
