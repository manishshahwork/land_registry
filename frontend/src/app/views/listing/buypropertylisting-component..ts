import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { BlockchainService } from '../../service/blockchain.service';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'buypropertylisting-component.html'
})
export class BuypropertylistingComponent implements OnInit {

  tableDataArr: any;

  constructor(private service: ServerService, private blockchain: BlockchainService) {
  }

  ngOnInit() {
    this.getAllBuypropertyFromDB();
    // if (Globals.shouldUseBlockchain())
    //   this.getAllSellersFromBlockchain();
    // else 
    // this.getAllBuypropertyFromDB();

  }

  getAllBuypropertyFromDB() {
    this.service.getAllBuyproperties().subscribe(
      data => {
        CustomLogger.logStringWithObject("getAllBuyproperties Resultsss from db: ", data);
        this.tableDataArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("getAllBuyproperties Error from db: ", err);
      }
    );
  }

  getAllSellersFromBlockchain() {
    this.blockchain.getSellersList().subscribe(
      data => {
        CustomLogger.logStringWithObject("getAllSellersFromBlockchain Result from BLOCKCHAIN: ", data);
        this.tableDataArr = data;
      },
      err => {
        CustomLogger.logStringWithObject("getAllSellersFromBlockchain Error from BLOCKCHAIN: ", err);
      }
    );
  }

  modifyTableData(event) {
    CustomLogger.logStringWithObject("Modify table data..", event);
  }


  onClickDelete(event) {
    CustomLogger.logStringWithObject("Delete table data..", event);
  }


}
