import { Component, OnInit } from '@angular/core';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular-6-social-login';
import { Router } from '@angular/router';
import { MyAuthService } from '../../service/myauth.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { UserProfile } from '../../others/models/userProfile.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';
import { ServerService } from '../../service/server.service';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { User } from '../../others/models/user.model';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  user: User;
  picklistUserTypes = CustomGlobalConstants.PICKLIST_USER_TYPES;
  picklistCompanyCategories = CustomGlobalConstants.PICKLIST_COMPANY_CATEGORIES;
  companyCategory = CustomGlobalConstants.PICKLIST_COMPANY_CATEGORIES[0];

  constructor(
    private socialAuthService: AuthService,
    private router: Router,
    private _myauth: MyAuthService,
    private _service: ServerService
  ) {
    CustomLogger.logString("Login Component check valid user (before): " + Globals.isUserValid());
    Globals.setUserValid(false);
    CustomLogger.logString("Login Component check valid user (after): " + Globals.isUserValid());
  }

  ngOnInit() {
    this.user = new User();
    this.user.type = CustomGlobalConstants.PICKLIST_USER_TYPES[0];
  }


  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        CustomLogger.logStringWithObject(socialPlatform + " sign in data : ", userData);
        let userProfile = new UserProfile();
        userProfile.name = userData.name;
        userProfile.google_id = userData.id;
        userProfile.email = userData.email;

        //make the default user role to be VENDOR
        userProfile.role = CustomGlobalConstants.USER_ROLE_VENDOR;

        CustomLogger.logStringWithObject("Authenticate UserProfile::: ", userProfile);
        this._myauth.authenticateGoogleUser(userProfile).subscribe(
          data => {
            CustomLogger.logStringWithObject("Authentication data: ", data);
            CustomLogger.logString("MESSAGE: " + data['message']);
            let token = data['message'];

            //STORE THE SECRET TOKEN
            localStorage.setItem(CustomGlobalConstants.KEY_TOKEN, token);

            //set up the globals to be used across the entire application
            this._service.getVendorGlobals(token).subscribe(
              data1 => {
                CustomLogger.logStringWithObject("data1: ", data1);
                let globalResult = data1["data"];
                Globals.setUserId(globalResult["user_uuid"]);
                Globals.setVendorId(globalResult["vendor_uuid"]);
                CustomLogger.logString("userid:: " + Globals.getUserId());
                CustomLogger.logString("vendorid:: " + Globals.getVendorId());

                //redirect the user based on his profile status
                let userFromDB = data["data"];
                CustomLogger.logStringWithObject("userFromDB:::", userFromDB);
                if (!userFromDB.has_completed_profile) {
                  CustomLogger.logString("should send to complete profile...");
                  Globals.setUserValid(false);
                  this.router.navigate(["profile"]);
                } else {
                  CustomLogger.logString("send to dashboard");
                  Globals.setUserValid(true);
                  this.router.navigate(["ourdashboard"]);
                }
              },
              err1 => {
                CustomLogger.logStringWithObject("err1: ", err1);
              }
            );
          },
          err => {
            CustomLogger.logStringWithObject("error: ", err);
          }
        )
      }
    );
  }


  onClickLogin() {
    CustomLogger.logStringWithObject("user to login: ", this.user);

    //flag to use blockchain
    Globals.useBlockchain(true);

    Globals.setLoginName(this.user.login_name);
    //authenticate the user
    this._service.isUserAuthenticated(this.user).subscribe(
      data => {
        CustomLogger.logStringWithObject("isUserAuthenticated: data: ", data);
        if (data["data"] != null) {
          Globals.setUserType(this.user.type);
          Globals.setUserId(data["data"].uuid);
          if (this.user.type == CustomGlobalConstants.CONSTANT_REALTOR || this.user.type == CustomGlobalConstants.CONSTANT_LAND_REGISTRY) {
            this.router.navigate(["ourdashboard"]);
            Globals.setUserTypeId(data["data"].uuid);
          } else {
            //based on userType get the specific userTypeId (i.e.Buyer id, Seller id, Realtor id etc.)
            this._service.getSpecificUserTypeId(this.user.type, data["data"].uuid).subscribe(
              data1 => {
                CustomLogger.logStringWithObject("getSpecificUserTypeId : data1: ", data1);
                Globals.setUserTypeId(data1["data"].uuid);
                this.router.navigate(["ourdashboard"]);
              },
              err1 => {
                CustomLogger.logStringWithObject("getSpecificUserTypeId: error: ", err1);
              }
            );
          }
        } else if ((this.user.type == CustomGlobalConstants.CONSTANT_REALTOR || this.user.type == CustomGlobalConstants.CONSTANT_LAND_REGISTRY) &&
          this.user.login_name == "admin" && this.user.password == "admin") {
          //create the admin login for realtor - default
          CustomLogger.logStringWithObject("current user: ", this.user);
          this.user.email = "landRegistryAdmin@kryptoblocks.io";
          this.user.password = "admin";
          this._service.saveNewUser(this.user).subscribe(
            data2 => {
              CustomLogger.logStringWithObject("saveNewUser : data2: ", data2);
              Globals.setUserTypeId(data2["data"].uuid);
              this.router.navigate(["ourdashboard"]);
            },
            err2 => {
              CustomLogger.logStringWithObject("saveNewUser error: ", err2);
            }
          );
        }

        else {
          alert("Invalid Login Credentials");
        }
      },
      err => {
        CustomLogger.logStringWithObject("isUserAuthenticated error: ", err);
      }
    );
  }

}//end of class
