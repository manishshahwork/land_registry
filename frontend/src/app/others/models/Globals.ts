export class Globals {
    private static is_valid: boolean = true;//should be false on startup
    private static user_id: string = "";
    private static vendor_id: string = "";
    private static company_category:string = "";
    private static user_type:string = "";
    private static user_type_id:string = "";
    private static login_name: string = "";
    private static use_blockchain:boolean = false;

    static isUserValid() {
        return this.is_valid;
    }

    static setUserValid(is_valid = false) {
        this.is_valid = is_valid;        
    }

    static getVendorId(){
        return this.vendor_id;
    }

    static setVendorId(vendorId){
        this.vendor_id = vendorId;
    }

    static getUserId(){
        return this.user_id;
    }
    static setUserId(userId){
        this.user_id = userId;
    }
    static getCompanyCategory(){
        return this.company_category;
    }
    static setCompanyCategory(companyCategory){
        this.company_category = companyCategory;
    }

    static getUserType(){
        return this.user_type;
    }
    static setUserType(userType){
        this.user_type = userType;
    }
    static getUserTypeId(){
        return this.user_type_id;
    }
    static setUserTypeId(userTypeId){
        this.user_type_id = userTypeId;
    }
    static getLoginName(){
        return this.login_name;
    }
    static setLoginName(loginName){
        this.login_name = loginName;
    }
    static useBlockchain(useBlockchain){
        this.use_blockchain = useBlockchain;
    }
    static shouldUseBlockchain(){
        return this.use_blockchain;
    }
}