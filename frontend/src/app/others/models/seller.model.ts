export class Seller {
    uuid: string;
    user_uuid: string;
    
    first_name: string = "";
    middle_name: string = "";
    last_name: string = "";
    spouse_first_name: string = "";
    spouse_middle_name: string = "";
    spouse_last_name: string = "";
    father_first_name: string = "";
    father_middle_name: string = "";
    father_last_name: string = "";
    mother_first_name: string = "";
    mother_middle_name: string = "";
    mother_last_name: string = "";
    street_address: string = "";
    city: string = "";
    state: string = "";
    country: string = "";
    phone_number: string = "";
    email: string = "";
    passport: string = "";
    DL: string = "";
    gov_id: string = "";
}