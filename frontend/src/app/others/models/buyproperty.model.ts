export class Buyproperty {
    uuid: string;
    buyer_uuid: string;
    property_uuid: string;
    property_name: string;
    seller_uuid: string;
    current_status: string; //closed/negotiation/cancelled - to be filled by seller
    buyer_remarks: string;
    seller_remarks: string;
    buyer_asking_price: string;
    buyer_name: string;
    seller_name: string;
    is_deleted: boolean = false;//once the deal is closed then delete it (keep it for logs)
    is_document_approved: boolean = false; //approved by land registry
    remarks_from_land_registry: string = "";
    agreement_document_name: string;
    agreement_document_url: string;

}