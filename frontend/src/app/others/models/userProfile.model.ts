export class UserProfile {
    uuid: string;//unique id
    name: string;
    role: string;
    facebook_id: string;
    google_id: string;
    phone: number;
    date_of_birth: string;
    gender: string;
    profile_link: string;
    email: string;
    created_on: number;
    last_modified: number;
    has_completed_profile: boolean;
}