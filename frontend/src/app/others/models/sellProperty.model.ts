export class SellProperty {
    uuid: string;
    property_uuid: string;
    buyer_uuid: string;
    seller_uuid: string;
    property_name: string;
    buyer_name: string;
    seller_name: string;
    commission: string;
    final_price: string;
    
}