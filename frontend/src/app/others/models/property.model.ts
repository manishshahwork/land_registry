export class Property {
    uuid: string;
    property_name: string;
    property_address: string;
    property_dimension: string;
    landmark: string;
    property_price: string;
    current_market_price: string;
    property_documents_hash: string;
    property_images_hash: string; string;
    property_video_hash: string;

    //owner
    seller_uuid: string;
    seller_name: string;
    property_type: string;

    //should be filled by land registry personnel
    registration_charges: string;
    stamp_duty_fees: string;
    gst: string;

    dispute_between_parties: string;
    description_of_disputes: string;

    list_of_cases: string;

    available_for_sale: boolean = true;

    //document upload
    document_name: string;
    document_type: string;
    document_location_url: string;
    
}