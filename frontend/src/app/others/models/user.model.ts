export class User {
    uuid: string;
    login_name: string;
    password: string;
    email: string;
    type: string;//Buyer/Seller/Bank/Realtor - (Realtor will be the admin)
}