const express = require("express");
const router = express.Router();
const Operations = require("../operations");
const ItemLog = require("../models/ItemLog")

//get all item logs
router.get("/getAll", async function (req, res) {
    logMsg("getAll");
    logObj(req.body);
    try {
        let itemLog = await Operations.getAllItemLogs();
        res.status(200).json({
            status: 200, message: 'Success in getting item', data: itemLog
        });
    } catch (err) {
        res.status(500).json({
            status: 400, message: 'Error in getting item', data: err
        });
    }
});

//add itemLog
router.post("/add", function (req, res) {
    logMsg("Item Log to be added ");
    logObj(req.body);
    logObj(req.params);
    let newItemLog = new ItemLog(req.body);
    logMsg("About to save itemlog:");
    logObj(newItemLog);
    try {
        Operations.addItemLog(newItemLog).then(reply => {
            logMsg("Item log added succesfully");
            logObj(reply);
            res.status(200).json({
                status: 200, message: 'Success in adding Item log', data: reply
            });
        });
    } catch (err) {
        logMsg("ERROR in adding Item log... ");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'Error in adding Item log'
        });
    }

    // Operations.addItemLog(newItemLog, function (err) {
    //     if (err) {
    //         res.json({ success: false, msg: 'Error in adding log: ' + err });
    //     } else {
    //         res.json({ success: true, msg: 'Item log added successfully: ' });
    //     }
    // });
});


//add array of itemLog
router.post("/addMany", async function (req, res) {
    logMsg("Many Item Logs to be added ");
    logObj(req.body);
    logObj(req.params);

    try {
        await Operations.addManyItemLogs(req.body).then(reply => {
            logMsg("Item logs added succesfully");
            logObj(reply);
            res.status(200).json({
                status: 200, message: 'Success in adding Item logs'
            });
        });
    } catch (err) {
        logMsg("ERROR in adding Item logs... ");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'Error in adding Item logs'
        });
    }
});

router.post("/updateManyItemLogs", async function (req, res) {
    logMsg("inside update many item logs data");
    logObj(req.body);
    logObj(req.params);

    try {
        await Operations.updateManyItemLogs(req.body);
        res.status(200).json({
            status: 200, message: 'Success in updating item logs'
        });
    } catch (err) {
        logMsg("ERROR in updating... ");
        logObj(err);
        res.json({ success: false, msg: 'Error in updating item logs: ' + err });
    }
})


//update itemLog
router.put("/update", function (req, res) {
    logMsg("Update item log");
    logObj(req.body);
    res.send("Item log Updated.");
});

//delete itemLog
router.post("/deleteOne", async function (req, res) {
    logMsg("Deleting item log");
    logObj(req.body);
    logObj(req.params);
    let id = req.body._id;
    try {
        await ItemLog.findByIdAndDelete(id);
        logMsg("item log deleted.");
        res.status(200).json({
            status: 200, message: 'Success in deleting'
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in deleting', data: error
        });
    }

})

//get itemLog by id
router.get("/getById/:id", async function (req, res) {
    logMsg("getById");
    logObj(req.body);
    logObj(req.params);
    let id = req.params.id;
    try {
        if (id == null) throw error("ID invalid");
        ItemLog.findById(id, function (err, result) {
            if (err) {
                res.status(500).json({
                    status: 500, message: 'Error in getting item', data: err
                });
            } else {
                res.status(200).json({
                    status: 200, message: 'Success in getting item', data: result
                });
            }
        })
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting item', data: err
        });
    }
});

// returns specific item logs
router.get('/search/:searchStartTime/:searchEndTime/:searchFeedbackType/:searchAnswerType', async function (req, res) {
    logMsg("Specific search for Item Log");
    logObj(req.body);
    logObj(req.params);
    try {
        let result = await Operations.getSpecificItemLogs(req.params.searchStartTime, req.params.searchEndTime, req.params.searchFeedbackType, req.params.searchAnswerType);
        logMsg("Success in search.");
        logObj(result);
        res.status(200).json({
            status: 200, message: 'Success in searching', data: result
        });
    } catch (err) {
        logMsg("Error in searching item log.");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'Error in getting item', data: err
        });
    }
});

//set like/dislike for a particular item log
router.post("/setLikeDislike", async function (req, res){
    logMsg("set like dislike..");
    logObj(req.body);
    logObj(req.params);
    try {
        let result = await Operations.setItemLogLikeDislike(req.body.itemLogId, req.body.isLike);
        logMsg("Success in setting like/dislike.");
        logObj(result);
        res.status(200).json({
            status: 200, message: 'Success in setting like/dislike.', data: result
        });
    } catch (err) {
        logMsg("Error in setting like/dislike.");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'setting like/dislike item log', data: err
        });
    }    
})

function logMsg(msg) {
    console.log("Item Log Route: " + msg);
}
function logObj(obj) {
    console.log("Item Log Route: ");
    console.log(obj);
}
//

module.exports = router; 