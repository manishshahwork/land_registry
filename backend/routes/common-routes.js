const express = require("express");
const router = express.Router();
const Operations = require("../operations");
const Property = require("../models/Property");
const Buyer = require("../models/Buyer");
const Seller = require("../models/Seller");
const SellProperty = require("../models/SellProperty");
// const PropertyReport = require("../models/PropertyReport");
const Buyproperty = require("../models/Buyproperty");
const User = require("../models/User");
const CustomGeocode = require("../models/CustomGeocode");

//properties
router.get("/getAllProperties", async function (req, res) {
    logMsg("getAllProperties");
    logObj(req.body);
    try {
        let result = await Property.find();
        res.status(200).json({
            status: 200, message: 'Success in getting all properties', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting all properties', data: err
        });
    }
});

router.post("/saveProperty", async function (req, res) {
    logMsg("saveProperty");
    logObj(req.body);
    try {
        let prop = new Property(req.body);
        prop.uuid = getNewUUID();
        let result = await prop.save();
        res.status(200).json({
            status: 200, message: 'Success in adding property', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding property', data: err
        });
    }
});

router.post("/updateProperty", async function (req, res) {
    logMsg("saveProperty");
    logObj(req.body);
    try {
        let result = await Property.updateOne({ 'uuid': req.body.uuid }, req.body);
        res.status(200).json({
            status: 200, message: 'Success in adding property', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding property', data: err
        });
    }
});

router.get("/getPropertyById/:property_uuid", async function (req, res) {
    logMsg("getPropertyById");
    logObj(req.params);
    try {
        let result = await Property.findOne({ 'uuid': req.params.property_uuid });
        res.status(200).json({
            status: 200, message: 'Success in getting property', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting property', data: err
        });
    }
});

router.get("/getSpecificProperties/:userType/:id", async function (req, res) {
    logMsg("getSpecificProperties: Entered");
    logObj(req.body);
    logObj(req.params);
    let userType = req.params.userType;
    let id = req.params.id;
    let result = null;
    try {
        if (userType == "Seller") {
            result = await Property.find({ 'seller_uuid': id, 'available_for_sale': true });
        } else if (userType == "Buyer") {
            result = await Property.find({ 'seller_uuid': id }); // buyer has already owned the property
        } else {
            result = await Property.find({ 'available_for_sale': true });
        }
        logMsg("Got result (getSpecificProperties): " + result);
        res.status(200).json({
            status: 200, message: 'Success in getSpecificProperties ', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getSpecificProperties', data: error
        });
    }
});

/////////////
//buyers
router.get("/getAllBuyers", async function (req, res) {
    logMsg("getAllBuyers");
    logObj(req.body);
    try {
        let result = await Buyer.find();
        res.status(200).json({
            status: 200, message: 'Success in getting all buyers', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting all Buyers', data: err
        });
    }
});

router.post("/saveBuyer", async function (req, res) {
    logMsg("save Buyer");
    logObj(req.body);
    try {
        let prop = new Buyer(req.body);
        prop.uuid = getNewUUID();
        let result = await prop.save();
        res.status(200).json({
            status: 200, message: 'Success in adding buyer', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding buyer', data: err
        });
    }
});

router.get("/getSingleBuyer/:buyer_uuid", async function (req, res) {
    logMsg("getSingleBuyer");
    logObj(req.params);
    try {
        let result = await Buyer.findOne({ 'uuid': req.params.buyer_uuid });
        res.status(200).json({
            status: 200, message: 'Success in getting Buyer', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting Buyer', data: err
        });
    }
});

/////////////
//sellers
router.get("/getAllSellers", async function (req, res) {
    logMsg("getAllSellers");
    logObj(req.body);
    try {
        let result = await Seller.find();
        res.status(200).json({
            status: 200, message: 'Success in getting all Sellers', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting all Sellers', data: err
        });
    }
});

router.post("/saveSeller", async function (req, res) {
    logMsg("save Seller");
    logObj(req.body);
    try {
        let prop = new Seller(req.body);
        prop.uuid = getNewUUID();
        let result = await prop.save();
        res.status(200).json({
            status: 200, message: 'Success in adding Seller', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding Seller', data: err
        });
    }
});

router.get("/getSingleSeller/:seller_uuid", async function (req, res) {
    logMsg("getSingleSeller");
    logObj(req.params);
    try {
        let result = await Seller.findOne({ 'uuid': req.params.seller_uuid });
        res.status(200).json({
            status: 200, message: 'Success in getting Seller', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting Seller', data: err
        });
    }
});
/////////////
//sell properties
router.get("/getAllSellProperties", async function (req, res) {
    logMsg("getAllSellProperties");
    logObj(req.body);
    try {
        let result = await SellProperty.find();
        res.status(200).json({
            status: 200, message: 'Success in getting all SellProperty', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting all SellProperty', data: err
        });
    }
});

router.post("/saveSellProperty", async function (req, res) {
    logMsg("saveSellProperty");
    logObj(req.body);
    try {
        let prop = new SellProperty(req.body);
        prop.uuid = getNewUUID();
        let result = await prop.save();
        res.status(200).json({
            status: 200, message: 'Success in adding SellProperty', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding SellProperty', data: error
        });
    }
});

router.get("/getSoldProperties/:userType/:id", async function (req, res) {
    logMsg("getSoldProperties");
    logObj(req.params);
    let userType = req.params.userType;
    let id = req.params.id;
    let result = null;
    try {
        if (userType == "Seller") {
            result = await SellProperty.find({ 'seller_uuid': id });
        } else if (userType == "Buyer") {
            result = await SellProperty.find({ 'buyer_uuid': id });
        }
        res.status(200).json({
            status: 200, message: 'Success in getSoldProperties', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getSoldProperties', data: err
        });
    }
});

//property report
router.post("/saveBuyproperty", async function (req, res) {
    logMsg("saveBuyproperty");
    logObj(req.body);
    try {
        let prop = new Buyproperty(req.body);
        prop.uuid = getNewUUID();
        let result = await prop.save();
        res.status(200).json({
            status: 200, message: 'Success in adding Buyproperty', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting adding Buyproperty', data: error
        });
    }
});

router.post("/updateBuyproperty", async function (req, res) {
    logMsg("updateBuyproperty");
    logObj(req.body);
    try {
        // let prop = new Buyproperty(req.body);
        // prop.uuid = getNewUUID();
        let result = await Buyproperty.updateOne({ 'uuid': req.body.uuid }, req.body);
        res.status(200).json({
            status: 200, message: 'Success in updateBuyproperty', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getting updateBuyproperty', data: error
        });
    }
});

router.get("/getAllBuyproperties", async function (req, res) {
    logMsg("getAllBuyproperties");
    try {
        let result = await Buyproperty.find();
        res.status(200).json({
            status: 200, message: 'Success in getting all Buyproperties', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting all Buyproperties', data: err
        });
    }
});

router.get("/getBuyPropertyById/:buyproperty_uuid", async function (req, res) {
    logMsg("getBuyPropertyById");
    logObj(req.params);
    try {
        let result = await Buyproperty.findOne({ 'uuid': req.params.buyproperty_uuid });
        res.status(200).json({
            status: 200, message: 'Success in getting getBuyPropertyById', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting getBuyPropertyById', data: err
        });
    }
});

router.get("/getBuyersBiddedProperties/:buyer_uuid", async function (req, res) {
    logMsg("getBuyersBiddedProperties");
    logObj(req.params);
    try {
        let result = await Buyproperty.find({ 'buyer_uuid': req.params.buyer_uuid, 'current_status': { $ne: 'Closed' } });
        res.status(200).json({
            status: 200, message: 'Success in getting getBuyersBiddedProperties', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting getBuyersBiddedProperties', data: err
        });
    }
});

router.get("/getSoldPropertiesOfSeller/:seller_uuid", async function (req, res) {
    logMsg("getSoldPropertiesOfSeller");
    logObj(req.params);
    try {
        let result = await Buyproperty.find({ 'seller_uuid': req.params.seller_uuid, 'current_status': 'Closed' });
        res.status(200).json({
            status: 200, message: 'Success in getting getSoldPropertiesOfSeller', data: result
        });
    } catch (err) {
        res.status(500).json({
            status: 500, message: 'Error in getting getSoldPropertiesOfSeller', data: err
        });
    }
});



//users
router.post("/saveNewUser", async function (req, res) {
    logMsg("saveNewUser: Entered");
    logObj(req.body);
    logObj(req.params);
    try {
        let user = new User(req.body);
        user.uuid = getNewUUID();
        let result = await user.save();
        res.status(200).json({
            status: 200, message: 'Success in saveNewUser', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in saveNewUser', data: error
        });
    }
});

router.post("/authenticateUser", async function (req, res) {
    logMsg("authenticateUser: Entered");
    logObj(req.body);
    logObj(req.params);
    try {
        let result = await User.findOne({ 'login_name': req.body.login_name, 'password': req.body.password, 'type': req.body.type });
        logMsg("authenticateUser: Got result;;;; " + result);

        // if (result == null) {
        //     //check if the username is admin and type is realtor.
        //     if (req.body.login_name == "admin" && req.body.type == "Realtor") {
        //         let user = new User(req.body);
        //         user.uuid = getNewUUID();
        //         result = await user.save();
        //     }
        // }
        res.status(200).json({
            status: 200, message: 'Success in authenticating user', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in authenticating user', data: error
        });
    }
});

router.get("/getSpecificUserTypeId/:userType/:user_uuid", async function (req, res) {
    logMsg("getSpecificUserTypeId: Entered");
    logObj(req.body);
    logObj(req.params);
    let userType = req.params.userType;
    let user_uuid = req.params.user_uuid;
    let result = null;
    try {
        if (userType == "Buyer") {
            result = await Buyer.findOne({ 'user_uuid': user_uuid });
        } else if (userType == "Seller") {
            result = await Seller.findOne({ 'user_uuid': user_uuid });
        } else {
            throw error("Invalid User Type");
        }
        logMsg("Got result;;;; " + result);
        res.status(200).json({
            status: 200, message: 'Success in getSpecificUserTypeId', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getSpecificUserTypeId', data: error
        });
    }
});


//property report
router.get("/getSpecificPropertyReport/:userType/:id", async function (req, res) {
    logMsg("getSpecificPropertyReport: Entered");
    logObj(req.body);
    logObj(req.params);
    let userType = req.params.userType;
    let id = req.params.id;
    let result = null;
    try {
        if (userType == "Seller") {
            result = await Buyproperty.find({ 'seller_uuid': id, 'is_deleted': false });
        } else if (userType == "Buyer") {
            result = await Buyproperty.find({ 'buyer_uuid': id, 'is_deleted': false });
        } else if (userType == "Land Registry") {
            result = await Buyproperty.find({ 'is_document_approved': false, 'current_status': 'Final' });
        } else {
            result = await Buyproperty.find();
        }
        logMsg("Got result;;;; " + result);
        res.status(200).json({
            status: 200, message: 'Success in getSpecificPropertyReport', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getSpecificPropertyReport', data: error
        });
    }
});


//land registry
router.get("/getLandRegistrySpecificPropertiesCount/:approvedFlag", async function (req, res) {
    logMsg("getLandRegistrySpecificPropertiesCount: Entered");
    logObj(req.body);
    logObj(req.params);
    let approvedFlag = req.params.approvedFlag;
    try {
        let result = await Buyproperty.find({ 'is_document_approved': approvedFlag });
        logMsg("getLandRegistrySpecificPropertiesCount: Got result: " + result);
        res.status(200).json({
            status: 200, message: 'Success in getLandRegistrySpecificPropertiesCount', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in getLandRegistrySpecificPropertiesCount', data: error
        });
    }
});


router.post("/addManyGeocode", async function (req, res) {
    logMsg("addManyGeocode: Entered");
    logObj(req.body);
    logObj(req.params);
    try {
        // let result = await CustomGeocode.insertMany(req.body);
        result = "aaaa";
        logMsg("addManyGeocode: Got result;;;; " + result);
        res.status(200).json({
            status: 200, message: 'Success in authenticating user', data: result
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in authenticating user', data: error
        });
    }
});

function getNewUUID() {
    return Date.now();
}

function logMsg(msg) {
    console.log("#### " + msg + " ####");
}
function logObj(obj) {
    console.log("########");
    console.log(obj);
}
//

module.exports = router; 