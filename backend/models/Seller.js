const mongoose = require("mongoose");

const SellerSchema = mongoose.Schema({
    uuid: { type: String },
    user_uuid: { type: String },
    first_name: { type: String },
    middle_name: { type: String },
    last_name: { type: String },
    spouse_first_name: { type: String },
    spouse_middle_name: { type: String },
    spouse_last_name: { type: String },
    father_first_name: { type: String },
    father_middle_name: { type: String },
    father_last_name: { type: String },
    mother_first_name: { type: String },
    mother_middle_name: { type: String },
    mother_last_name: { type: String },
    street_address: { type: String },
    city: { type: String },
    state: { type: String },
    country: { type: String },
    phone_number: { type: String },
    email: { type: String },
    passport: { type: String },
    DL: { type: String },
    gov_id: { type: String }
});

const Seller = module.exports = mongoose.model("Seller", SellerSchema, "sellers");
