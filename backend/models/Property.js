const mongoose = require("mongoose");

const PropertySchema = mongoose.Schema({
    uuid: { type: String },
    property_name: { type: String },
    property_address: { type: String },
    property_dimension: { type: String },
    landmark: { type: String },
    property_price: { type: String },
    current_market_price: { type: String },
    property_documents_hash: { type: String },
    property_images_hash: { type: String }, 
    property_video_hash: { type: String },

    seller_uuid: { type: String },
    seller_name: { type: String },
    property_type: { type: String },

    registration_charges: { type: String },
    stamp_duty_fees: { type: String },
    gst: { type: String },

    dispute_between_parties: { type: String },
    description_of_disputes: { type: String },

    list_of_cases: { type: String },

    available_for_sale: { type: Boolean },

    document_name: { type: String },
    document_type: { type: String },
    document_location_url: { type: String }

    
});

const Property = module.exports = mongoose.model("Property", PropertySchema, "properties");
