const mongoose = require("mongoose");

const CustomGeocodeSchema = mongoose.Schema({
    country: { type: String },
    city: { type: String },
    lat: { type: String },
    lng: { type: String }
});

const CustomGeocode = module.exports = mongoose.model("CustomGeocode", CustomGeocodeSchema, "custom_geocode");
