const mongoose = require("mongoose");

const SellPropertySchema = mongoose.Schema({
    uuid: { type: String },
    property_uuid: { type: String },
    buyer_uuid: { type: String },
    seller_uuid: { type: String },
    property_name: { type: String },
    buyer_name: { type: String },
    seller_name: { type: String },
    commission: { type: String },
    final_price: { type: String }
});

const SellProperty = module.exports = mongoose.model("SellProperty", SellPropertySchema, "sell_properties");
