const mongoose = require("mongoose");

const ItemLogSchema = mongoose.Schema({
    log_date: { type: Number },
    question: { type: String },
    answer: { type: String },
    feedback: { type: String, default:"" },
    sme: { type: String, default:"" },
    is_answered: {type: Boolean},
    is_liked: {type:Boolean}
});

const ItemLog = module.exports = mongoose.model("ItemLog", ItemLogSchema, "itemlogs");
