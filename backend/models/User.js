const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    
    uuid: { type: String },
    login_name: { type: String },
    password: { type: String },
    email: { type: String },
    type: { type: String }
});

const User = module.exports = mongoose.model("User", UserSchema, "users");
