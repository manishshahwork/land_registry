const mongoose = require("mongoose");

const BuypropertySchema = mongoose.Schema({
    uuid: { type: String },
    buyer_uuid: { type: String },
    property_uuid: { type: String },
    property_name: { type: String },
    seller_uuid: { type: String },
    current_status: { type: String }, //closed/negotiation/cancelled - to be filled by seller
    buyer_remarks: { type: String },
    seller_remarks: { type: String },
    buyer_asking_price: { type: String },
    buyer_name: { type: String },
    seller_name: { type: String },
    is_deleted: { type: Boolean },
    is_document_approved: { type: Boolean },
    remarks_from_land_registry: { type: String },
    agreement_document_name: { type: String },
    agreement_document_url: { type: String }

});

const Buyproperty = module.exports = mongoose.model("Buyproperty", BuypropertySchema, "buyproperties");
