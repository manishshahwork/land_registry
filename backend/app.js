const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config/db");
const app = express();


//connect to database
mongoose.connect(config.db_url, { useNewUrlParser: true });

//check if connected
mongoose.connection.on('connected', function (){
    console.log("Connected to database: " + config.db_url);
});

//check if error in db connection
mongoose.connection.on('error', function (err){
    console.log("Error in connecting database: " + err);
});


const commonRoutes = require('./routes/common-routes');

//all '/itemLogs' request to be sent here
const itemLogsRoutes = require('./routes/itemLogs-routes');
//all '/trainingData' request to be sent here
const trainingDataRoutes = require('./routes/trainingData-routes');

//port number
const port = 3000;

//allow all domain to access this
//CORS middleware
app.use(cors());

//body parser middleware
app.use(bodyParser.json());

//route 
app.use('/common', commonRoutes);
app.use('/itemLogs', itemLogsRoutes);
app.use('/trainingData', trainingDataRoutes);

//dummy response
//index route
app.get("/", function(req, res){
    res.send("Invalid Endpoint");
});

//start server
app.listen(port, function(){
    console.log("Server started on port: " + port);
});



